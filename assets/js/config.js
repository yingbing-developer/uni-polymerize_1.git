const bookTypes = {
	single: {
		novel: '小说',
		comic: '漫画'
	},
	gather: {
		novels: '书单',
		comics: '漫单'
	}
}
const pictureTypes = {
	single: {
		wallpaper: '壁纸',
		figure: '套图',
		portrait: '写真'
	},
	gather: {
		wallpapers: '壁纸集',
		figures: '套图集',
		portraits: '写真集'
	}
}
const videoTypes = {
	single: {
		movie: '电影',
		drama: '连续剧',
		anime: '番剧',
		variety: '综艺',
		short: '短视频',
		mv: 'MV'
	},
	gather: {
		movies: '电影集',
		dramas: '连续剧集',
		animes: '番剧集',
		varietys: '综艺集',
		shorts: '短片集',
		mvs: 'MV集',
	}
}
const audioTypes = {
	single: {
		song: '歌曲',
		program: '节目'
	},
	gather: {
		songlist: '歌单',
		songalbum: '专辑',
		audible: '有声书',
	}
}
const communityTypes = {
	single: {
		article: '文章',
		news: '资讯',
		talk: '话题'
	},
	gather: {
		articles: '文章集',
		newss: '新闻集',
		talks: '话题集',
	}
}
const liveTypes = {
	single: {
		live: '直播',
		broadcast: '广播'
	},
	gather: {
		lives: '直播集',
		broadcasts: '广播集'
	}
}
const characterTypes = {
	single: {
		singer: '歌手',
		writer: '写手',
		uploader: 'UP',
		anchor: '主播',
		painter: '画师',
		blogger: '博主',
		actor: '演员',
		announcer: '播音员'
	},
	gather: {
		
	}
}
const relation = {
	novel: {
		label: '小说',
		single: 'novel',
		gather: 'novels'
	},
	comic: {
		label: '漫画',
		single: 'comic',
		gather: 'comics'
	},
	wallpaper: {
		label: '壁纸',
		single: 'wallpaper',
		gather: 'wallpapers'
	},
	figure: {
		label: '套图',
		single: 'figure',
		gather: 'figures'
	},
	portrait: {
		label: '写真',
		single: 'portrait',
		gather: 'portraits'
	},
	movie: {
		label: '电影',
		single: 'movie',
		gather: 'movies'
	},
	drama: {
		label: '连续剧',
		single: 'drama',
		gather: 'dramas'
	},
	anime: {
		label: '番剧',
		single: 'anime',
		gather: 'animes'
	},
	variety: {
		label: '综艺',
		single: 'variety',
		gather: 'varietys'
	},
	short: {
		label: '短视频',
		single: 'short',
		gather: 'shorts'
	},
	mv: {
		label: 'MV',
		single: 'mv',
		gather: 'mvs'
	},
	live: {
		label: '直播',
		single: 'live',
		gather: 'lives'
	},
	broadcast: {
		label: '广播',
		single: 'broadcast',
		gather: 'broadcasts'
	},
	music: {
		label: '音乐',
		single: 'song',
		gather: 'songlist,songalbum'
	},
	audible: {
		label: '有声书',
		single: 'program',
		gather: 'audible'
	},
	article: {
		label: '文章',
		single: 'article',
		gather: 'articles'
	},
	news: {
		label: '资讯',
		single: 'news',
		gather: 'newss'
	},
	forum: {
		label: '社区',
		single: 'talk',
		gather: 'talks'
	},
	character: {
		label: '用户',
		single: 'writer,painter,uploader,anchor,singer,blogger,actor,announcer'
	}
}
export default {
	//请求成功编码
	ERR_OK: 200,
	//请求失败编码
	ERR_FALSE: 300,
	//请求超时时间
	TIMEOUT: 50000,
	//播放器
	videoplayer: {
		dcloudplayer: 'NVUE播放器',
		ybplayer: 'H5播放器'
	},
	//排序表
	sorts: [{
		label: '默认排序',
		value: 'default'
	},{
		label: '最近更新',
		value: 'newest'
	},{
		label: '最多人看',
		value: 'most'
	},{
		label: '热度最高',
		value: 'hottest'
	}],
	//发现页排序
	findSorts: [{
		label: '推荐',
		value: 'recome'
	},{
		label: '同城',
		value: 'samecity'
	}],
	//分区关系表
	relation: relation,
	//整合类型
	conTypes: {
		book: '书籍',
		picture: '图片',
		video: '视频',
		live: '实时',
		audio: '音频',
		community: '文章',
		character: '用户'
	},
	//整合关系表
	conRelation: {
		book: Object.keys(bookTypes.single).concat(Object.keys(bookTypes.gather)),
		picture: Object.keys(pictureTypes.single).concat(Object.keys(pictureTypes.gather)),
		video: Object.keys(videoTypes.single).concat(Object.keys(videoTypes.gather)),
		live: Object.keys(liveTypes.single).concat(Object.keys(liveTypes.gather)),
		audio: Object.keys(audioTypes.single).concat(Object.keys(audioTypes.gather)),
		community: Object.keys(communityTypes.single).concat(Object.keys(communityTypes.gather)),
		character: Object.keys(characterTypes.single),
	},
	//书籍类型
	bookTypes: bookTypes,
	//图片类型
	pictureTypes: pictureTypes,
	//视频类型
	videoTypes: videoTypes,
	//音频类型
	audioTypes: audioTypes,
	//社区博客类型
	communityTypes: communityTypes,
	//直播类型
	liveTypes: liveTypes,
	//单个资源类型
	singleTypes: Object.assign({}, bookTypes.single, pictureTypes.single, videoTypes.single, audioTypes.single, communityTypes.single, liveTypes.single),
	//集合类型
	gatherTypes: Object.assign({}, bookTypes.gather, pictureTypes.gather, videoTypes.gather, audioTypes.gather, communityTypes.gather, liveTypes.gather, {rank: '榜单'}, characterTypes.gather),
	//角色类型
	characterTypes: characterTypes,
	app: {
		name: '好用聚合',
		logo: '/static/logo.png'
	}
}
