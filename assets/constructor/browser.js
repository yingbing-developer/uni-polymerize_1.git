//浏览器书签/历史
import Utils from '@/uni_modules/yingbing-ui/js_sdk/util.js'

const { dateFormat } = Utils;

export default class Browser {
	constructor({id, title, src, type, isAdult, createTime}) {
		this.id = id || new Date().getTime().toString() + Math.round(Math.random() * 10000) + '_' + this.type
		this.title = title
		this.src = src
		this.type = type
		this.isAdult = isAdult || false
		this.createTime = createTime || dateFormat(new Date().getTime())
	}
}