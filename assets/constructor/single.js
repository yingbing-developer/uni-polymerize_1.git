//资源
import Utils from '@/uni_modules/yingbing-ui/js_sdk/util.js'

const { dateFormat } = Utils;

export default class Single {
	constructor({id, title, singleId, commentId, lyricId, barrageId, src, comments, characters, lyrics, barrages, list, sections, downloadUrl, shareUrl, cover, creator, avatar, desc, content, isPay, isLiving, readnum, popular, collect, comment, thumbs, isEnd, latest, duration, size, style, vertical, tag, isAdult, record, mark, fontFace, extra, formats, type, createTime, updateTime, source }) {
		this.id = id || ((singleId || '') + '_' + source)//资源唯一ID 必填 如果填了singleId 可以不填
	  	this.title = title || ''//名称
	  	this.singleId = singleId || ''//资源ID
	  	this.commentId = commentId || ''//评论ID
	  	this.lyricId = lyricId || ''//歌词ID
	  	this.barrageId = barrageId || ''//弹幕ID
	  	this.src = src || ''//路径
		this.comments = comments || []//评论列表
		this.characters = characters || []//相关用户列表
		this.lyrics = lyrics || []//歌词
		this.barrages = barrages || []//弹幕
		this.list = list || []//相关作品列表（可以是图片链接，也可以是歌曲或者其它资源）
		this.sections = sections || []//分段内容（例如章节，分集等）
		this.downloadUrl = downloadUrl || ''//下载链接
		this.shareUrl = shareUrl || ''//分享链接
	  	this.cover = cover || ''//封面
	  	this.creator = creator || ''//创作者
		this.avatar = avatar || ''//创作者头像
	  	this.desc = desc || ''//介绍
	  	this.content = content || ''//内容
	  	this.isPay = isPay || false//是否付费
	  	this.isLiving = isLiving?.toString() == 'false' ? false : (isLiving || true)//正在直播
	  	this.readnum = readnum || 0//阅读次数
	  	this.popular = popular || 0//热度
	  	this.collect = collect || 0//收藏数
	  	this.comment = comment || 0//评论数
	  	this.thumbs = thumbs || 0//点赞数
		this.isEnd = isEnd || false//是否完结
		this.latest = latest || ''//最新更新
		this.duration = duration || 0//播放时长
		this.size = size || ''//大小
		this.style = style || ''//风格
		this.vertical = vertical || false//是否竖屏
		this.tag = tag || []//标签
		this.isAdult = isAdult || false//是否青壮年内容
		this.record = record || {}//记录
		this.mark = mark || []//书签
		this.fontFace = fontFace || {}//自定义字体
	  	this.extra = extra || {}//附加字段
		this.formats = formats || 'auto'//视频格式
	  	this.type = type//资源类型
		this.createTime = createTime || dateFormat(new Date().getTime())//创建时间
		this.updateTime = updateTime || ''//更新时间
	  	this.source = source || 'local'//来源
	}
}