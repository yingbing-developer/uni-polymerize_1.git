//分段

export default class Section {
	constructor({id, sectionId, commentId, barrageId, title, chapter, creator, src, barrages, cover, size, desc, isStart, isEnd, content, vertical, isPay, isAdult, formats, startTime, endTime, type, extra, source}) {
		this.id = id || ((sectionId || '') + '_' + source)//资源唯一ID 必填 如果填了sectionId 可以不填
		this.sectionId = sectionId || ''//分段ID
		this.commentId = commentId || ''//评论ID
		this.barrageId = barrageId || ''//弹幕ID
		this.title = title || ''//标题
		this.chapter = chapter//序号
		this.creator = creator || ''//创建者
		this.src = src || ''//链接
		this.barrages = barrages || []//弹幕集合
		this.cover = cover || ''//封面
		this.size = size || ''//大小
		this.desc = desc || ''//介绍
		this.isStart = isStart || false
		this.isEnd = isEnd || false
		this.content = content || null
		this.vertical = vertical || false//是否竖屏
		this.isPay = isPay || false//否需要支付观看
		this.isAdult = isAdult || false//是否青壮年内容
		this.formats = formats || 'auto'//视频格式
		this.startTime = startTime || 0//开始时间
		this.endTime = endTime || 0//结束时间
		this.type = type || ''//类型
		this.extra = extra || {}//附加字段
		this.source = source//来源
	}
}