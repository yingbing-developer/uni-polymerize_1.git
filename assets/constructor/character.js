//角色
import Utils from '@/uni_modules/yingbing-ui/js_sdk/util.js'

const { dateFormat } = Utils;

export default class Character {
	constructor({id, characterId, commentId, followeeId, followerId, name, avatar, cover, desc, list, followee, follower, works, style, isAdult, tag, extra, type, createTime, updateTime, source}) {
		this.id = id || ((characterId || '') + '_' + source)//资源唯一ID 必填 如果填了characterId 可以不填
	  	this.characterId = characterId || ''//角色ID
	  	this.commentId = commentId || ''//评论ID
	  	this.followeeId = followeeId || ''//关注者列表ID
		this.followerId = followerId || ''//跟随者列表ID
	  	this.name = name || '佚名'//名称
	  	this.avatar = avatar || ''//头像
	  	this.cover = cover || ''//封面
	  	this.desc = desc || ''//介绍
	  	this.list = list || []//作品集合
		this.followee = followee || 0//关注者数量
		this.follower = follower || 0//跟随者数量
		this.works = works || 0//作品数量
		this.style = style || ''//风格
		this.isAdult = isAdult || false//是否青壮年内容
		this.tag = tag || []//标签
	  	this.extra = extra || {}//附加字段
	  	this.type = type//用户类型
		this.createTime = createTime || dateFormat(new Date().getTime())//创建时间
		this.updateTime = updateTime || ''//更新时间
	  	this.source = source//来源
	}
}