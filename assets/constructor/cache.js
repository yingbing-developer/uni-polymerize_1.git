//文件缓存

export default class Cache {
	constructor({parentId, parent, title, cover, src, bytes, type, extra, source}) {
	  this.id = parentId + '_cache'
	  this.parentId = parentId//父id
	  this.parent = parent || null //内容
	  this.title = title || ''//文件名称
	  this.cover = cover || ''//封面
	  this.src = src//文件路径
	  this.bytes = bytes//文件总量
	  this.type = type || ''//文件类型
	  this.extra = extra || {}//附加字段
	  this.source = source || ''//来源
	}
}