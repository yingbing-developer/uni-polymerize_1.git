//评论

export default class Comment {
	constructor({id, commentId, avatar, title, subtitle, content, subComment, masterComment, extra, source}) {
		this.id = id || ((commentId || '') + '_' + source)
		this.commentId = commentId || ''
		this.avatar = avatar || ''
		this.title = title || ''
		this.subtitle = subtitle || ''
		this.content = content || ''
		this.subComment = subComment || []
		this.masterComment = masterComment || []
		this.type = 'comment'//类型
		this.extra = extra || {}//附加字段
		this.source = source || 'local'//来源
	}
}