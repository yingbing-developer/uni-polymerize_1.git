export default const CSSCoder = {
	format : function(s){//格式化代码
		s=s.replace(/\s*([\{\}\:\;\,])\s*/g,"$1");
		s=s.replace(/\,[\s\.\#\d]*{/g,"{");
		s=s.replace(/([^\s])\{([^\s])/g,"$1 {\n\t$2");
		s=s.replace(/([^\s])\}([^\n]*)/g,"$1\n}\n$2");
		s=s.replace(/([^\s]);([^\s\}])/g,"$1;\n\t$2");
		s=s.replace(/;\s*;/g,";");//清除连续分号
		return s;
	},
	pack :function(s){//压缩代码
		s=s.replace(/\/\*(.|\n)*?\*\//g,"");//删除注释
		s=s.replace(/\s*([\{\}\:\;\,\[\s\.\#\d])*\{/g, "{");//容错处理
		s=s.replace(/;\s*;/g,";");//清除连续分号
		s=s.replace(/[\t\r\n]/g,"");//清除连续分号
		return s;
	}
}