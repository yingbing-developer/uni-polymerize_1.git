import Xhr from './xhr.js'
import Http from './http.js'
export default {
	xhr: new Xhr(),
	http: new Http()
}