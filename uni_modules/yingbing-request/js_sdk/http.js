import GBK from './charset/gbk.js'
let requestTasks = [];

//request封装
function request (type = 'GET', url, options) {
	return new Promise((resolve,reject) => {
		let d = new Date();
		let taskId = options.taskId || (d.getMinutes().toString() + d.getSeconds() + d.getMilliseconds() + Math.round(Math.random() * 10000))
		// #ifdef H5
		if ( options.headers ) {
			Reflect.deleteProperty(options.headers, 'Referer');
			Reflect.deleteProperty(options.headers, 'referer');
			Reflect.deleteProperty(options.headers, 'Host');
			Reflect.deleteProperty(options.headers, 'host');
			Reflect.deleteProperty(options.headers, 'user-agent');
			Reflect.deleteProperty(options.headers, 'UserAgent');
		}
		// #endif
		requestTasks.push({
			id: taskId,
			request: uni.request({
				url: url,
				data: charsetParams(options.params || {}, options.headers?.Charset),
				method: type || 'GET',
				mimeType: options.mimeType || null,
				dataType: options.dataType || 'json',
				header: options.headers || {},
				responseType: options.responseType || 'text',
				timeout: options.timeout || 30000,
				sslVerify: false,
				success: ((res) => {
					resolve(res)
				}),
				fail:((err)=>{
					reject(err)
				}),
				complete: (() => {
					let index = requestTasks.findIndex(task => task.id == taskId)
					index > -1 ? requestTasks.splice(index, 1) : null
				})
			})
		})
	})
}

export default class http {
	get(url, options = {}) {
		return request('GET', urlPrams(url, options), options)
	}
	postget(url, options = {}) {
		return request('POST', urlPrams(url, options), options)
	}
	post(url, options = {}) {
		return request('POST', url, options)
	}
	getAll(reqs) {
		return Promise.all(
			reqs.map(req => {
				return this.get(urlPrams(req.url, req.options), req?.options || {})
			})
		)
	}
	postgetAll(reqs) {
		return Promise.all(
			reqs.map(req => {
				return this.postget(urlPrams(req.url, req.options), req?.options || {})
			})
		)
	}
	postAll(reqs) {
		return Promise.all(
			reqs.map(req => {
				return this.post(req.url, req?.options || {})
			})
		)
	}
	abort(taskIds) {
		taskIds.split(',').forEach(taskId => {
			let index = requestTasks.findIndex(task => task.id == taskId)
			if ( index > -1 ) {
				requestTasks[index].request.abort()
			} else {
				throw new Error('The requested task with taskid ' + taskId + ' is not defined')
			}
		})
	}
}

function param(data, charset = 'utf-8') {
    let url = ''
	let params = charsetParams(data, charset)
    for (var k in params) {
        let value = params[k] !== undefined ? params[k] : ''
        url += `&${k}=${ charset == 'utf-8' ? encodeURIComponent(value) : value}`
    }
    return url ? url.substring(1) : ''
}

function urlPrams (url, options = {}) {
	url += (url.indexOf('?') < 0 ? '?' : '&') + param(options.params || {}, options.headers?.Charset || 'utf-8') || ''
    return url
}

function charsetParams (params, charset = 'utf-8') {
	Object.keys(params).forEach(key => {
		params[key] = (charset == 'utf-8' || charset == 'UTF-8') ? params[key] : (charset.indexOf('gb') > -1 || charset.indexOf('GB') > - 1) ? GBK(params[key]) : params[key]
	})
	return params
}