#使用须知

* 1、这是一个小说分页插件，不包含设置窗口
* 2、这个插件支持APP-NVUE、APP-VUE、H5、微信小程序
* 3、插件本身只支持滚动阅读，如果需要翻页功要配合[好用翻页插件](https://ext.dcloud.net.cn/plugin?id=13712)同时下载使用
* 4、小说分页有2种模式，章节模式和整书模式，章节模式就是需要分章节加载的小说，整书模式就是不分章节直接传入整本小说的模式
* 5、小说内容只支持纯文本格式 ，例如（内容内容内容内容内容/r/n内容内容内容内容内容）
* 6、章节模式建议初始化内容和跳转章节时一次传3个章节的内容
* 7、自定义页面有图片的，最好将图片高度固定，避免滚动模式下定位异常，且自定义页面只能放入章节后面展示
* 8、h5页面电量显示只能在安全环境下获取（比如：https://、file:///、localhost://）
* 9、不同平台下的体验是不同的，功能也有些许差别，比如APP-NVUE支持css较少，所以翻页效果不如其它端
* 10、有什么不懂，可以加群 1087735942 聊

#props属性
| 属性名 | 类型 | 默认值 | 可选值 | 说明 |
| :----- | :----: | :----: | :----: | :---- |
| autoplay | Boolean | false | true/false | 自动翻页 |
| pageType | String | scroll | real/cover/scroll/none | 翻页模式 |
| color | String | #333333 | ---- | 字体颜色 |
| fontSize | String/Number | 15 | ---- | 字体大小 |
| fontFamily | String | 'Microsoft YaHei, 微软雅黑' | ---- | 字体名称 |
| measureSize | Object | ---- | ---- | 计算尺寸（具体见下面） |
| fontFace | Array | ---- | ---- | 加载自定义字体 |
| bgColor | String | #fcd281 | ---- | 背景颜色（支持css渐变） |
| lineHeight | String/Number | 15 | ---- | 行间距（行与行之间的间距） |
| slide | String/Number | 40 | ---- | 页面左右边距 |
| topGap | String/Number | 10 | ---- | 页面上边距 |
| bottomGap | String/Number | 10 | ---- | 页面下边距 |
| headerShow | Boolean | true | true/false | 显示顶部内容 |
| footerShow | Boolean | true | true/false | 显示底部内容 |
| firstTip | String | 前面已经没有了 | ---- | 翻到第一页得提示文字 |
| lastTip | String | 后面已经没有了 | ---- | 翻到最后一页得提示文字 |
| firstTipUnable | Boolean | false | true/false | 关闭第一页提示 |
| lastTipUnable | Boolean | false | true/false | 关闭最后一页提示 |
| noChapter | Boolean | false | true/false | 是否开启整书模式（无章节模式） |
| enablePreload | Boolean | false | true/false | 是否开启预加载章节功能（noChapter为false时有效） |
| enableClick | Boolean | false | true/false | 是否开启点击区域（用于唤出设置窗口之类） |
| unableClickPage | Boolean | false | true/false | 是否关闭点击左右2边位置翻页（pageType为none时忽略此属性） |
| clickOption | Object | { width: uni.upx2px(200),height: uni.upx2px(200),left:'auto',top:'auto'} | ---- | 点击区域配置（点击哪个区域有效 enableClick为true时有效） |

#clickOption
| 键名 | 类型 | 说明 |
| :----- | :----: | :---- |
| width | Number | 点击区域宽度 |
| height | Number | 点击区域高度 |
| left | String/Number | 左右定位（默认auto为居中，可传入数字） |
| top | String/Number | 上下定位（默认auto为居中，可传入数字） |

#measureSize（用于分页计算，一般不用填，插件内部会自动测量，如果测量不对，可以自行填入修正）
| 键名 | 类型 | 默认值 | 说明 |
| :----- | :----: | :----: | :---- |
| letter | Number | 7 | 单个英文字母宽度 |
| number | Number | 5.5 | 单个数字宽度  |
| chinese | Number | 10 | 单个中文宽度  |
| space | Number | 3.5 | 单个空格宽度 |
| special | Number | 8 | 单个特殊字符宽度 |
| other | Number | 10 | 其余字符宽度 |

#event事件
| 事件名 | 参数 | 说明 |
| :----- | :----: | :---- |
| loadmore | chapter,callback | 加载章节内容（chapter为需要加载的章节序号，callback为加载回调 此方法在noChapter为false有效）|
| preload | chapters,callback | 预加载章节内容（chapters为需要预加载的章节序号集合，callback为加载回调 此方法在noChapter为false有效）|
| change | currentInfo | 阅读页面改变触发事件（返回当前阅读页面信息）|
| setCatalog | catalog | 获取章节目录事件（此事件在noChapter为true时有效）|
| clickTo | ---- | 点击事件（此事件在enableClick为true时有效）|
| 自定义 | ---- | 自定义页面中的点击事件回馈（你写了多少个点击事件，就有多少个event事件） |

#内置方法
| 方法名 | 参数 | 说明 |
| :--- | :------ | :---- |
| init | { contents: '章节模式下是小说内容集合，整书模式下是小说文本内容', currentChapter: '小说定位章节序号', start: '定位章节的开始阅读开始位置' } | 初始化小说内容 |
| change | { contents: '小说内容集合（整书模式下不传，章节模式非必传）', currentChapter: '小说定位章节序号', start: '定位章节的开始阅读开始位置' } | 跳转小说位置 |
| refresh | ---- | 刷新阅读页面 |
| pageNext | ---- | 翻到下一页 （滚动模式下表现为向下滚动一段距离） |
| pagePrev | ---- | 翻到上一页 （滚动模式下表现为向上滚动一段距离） |

#content对象介绍
| 键名 | 类型 | 说明 |
| :----- | :----: | :---- |
| chapter | Number | 章节序号 |
| content | String | 章节内容 |
| custom | Array | 自定义内容（具体用法见下面） |
| isEnd | Boolean | 是否是最后一个章节 |
| isStart | Boolean | 是否是第一章节 |
| title | String | 章节名称（非必传）如果传了得话，会在currentChange中返回 |

#loadmore和preload事件回调callback介绍
| 参数 | 类型 | 是否必传 | 可选值 | 说明 |
| :----- | :----: | :---- |
| status | String | 是 | success/fail/timeout | 请求回调状态 |
| content/contents | Object | 是 | loadmore方法需要传入content对象, preload方法需要传入content对象集合contents | 请求回调内容 |

#currentChange事件参数currentInfo介绍
| 键名 | 类型  | 说明 |
| :----- | :----: | :---- |
| chapter | String | 当前页面所在章节 |
| start | Number | 当前页面所在章节的阅读开始位置 |
| end | Number | 当前页面所在章节的阅读结束位置 |
| dataId | Number | 插件内部使用参数不用处理 |
| type | String | 插件内部使用参数（可以通过此属性判断当前页面是自定义页面还是文本页面） |
| text | Array | 当前页面文字集合 |
| totalPage | Number | 当前章节的全部分页数量 |
| currentPage | Number | 当前章节第几页 |
| title | String | 章节名称（如果content对象中带有title才会返回这个参数） |

#slot插槽
| 名称  | 说明 | 兼容 |
| :----- | :---- | :---- |
| top | 自定义第一页提示页面 | 滚动模式下不支持 |
| bottom | 自定义最后一页提示页面 | 滚动模式下不支持 |


#初始化插件
```html
	<yingbing-ReadPage ref="page" style="height: 100vh;"></yingbing-ReadPage>
```
```javascript
	export default {
		onReady () {
			let contents = [{
			    chapter: 1,//章节序号
			    content: this.getContent(1),//文本内容
			    title: '第一章',//章节标题
			    isStart: false,//是否是第一章节
			    isEnd: false,//是否是最后一个章节
			},{
			    chapter: 2,
			    content: this.getContent(2),
			    title: '第二章',
			    isStart: false,
			    isEnd: false,
			},{
			    chapter: 3,
			    content: this.getContent(3),
			    title: '第三章',
			    isStart: false,
			    isEnd: false,
			}]
			const { page } = this.$refs;
			page.init({
			    contents: contents,
			    currentChapter: 2,//从第二章节开始阅读 和章节chapter对应
				start: 100//从章节的第100个字开始阅读
			})
		},
		methods: {
			getContent (chapter = 1) {
				return `第${chapter}章
				　　灼烫或红中发黑的文字是必须执行的剧情，否则只有死路一条。\n　　作为可以交易鬼物的死魂纸，可以根据该人探知真相进度的贡献度来获得，而篡改关键性剧情，会没收一定数量的死魂纸。\n　　但这并不代表扮演者要完全忠诚地按照剧本说的去做，若果真如此，也只有死这一条路可走！相反，往往需要扮演者打破剧本中固有的剧情，才能够赢得一丝生机！\n　　页面上浮现了一行血字：\n　　【是否打开鬼物商城？】\n　　是。\n　　陈献的眼前出现了一个黑色的页面，边框熊熊燃烧着地狱之火，炙人眼球。\n　　大概二十多个鬼物被挂在页面上，几乎都是动辄需要一两千张死魂纸的鬼物。\n　　目前陈献的100张死魂纸，只够购买一个叫【死亡人眼】的鬼物。\n　　【死亡人眼】：共可使用三次，已被使用两次，剩余可击退一次轻级诅咒鬼魂的攻击，对中级诅咒鬼魂有轻微抑制性作用，对高级和顶级诅咒鬼魂无用。卖家：盲人女孩扮演者凌小薇。（在《猫脸老太太》恐怖剧本中可获得）\n　　页面的血字发生了改变。\n　　【是否使用二十张死魂纸刷新页面？】\n　　现在这个页面他能勉强买的起的就只有一个【死亡人眼】，再用20张死魂纸刷新，恐怕一个也买不了。\n　　陈献当机立断，购买了【死亡人眼】\n　　【鬼物：死亡人眼\n　　死魂纸数量：0】\n　　交易的瞬间，他的手心出现了一种圆润滑腻的触感，带着股若有若无的腥臊味。\n　　陈献打开手心一看，里面有一只玻璃球大小的眼球，上眼皮黏连在上面，很像荔枝，而眼球的瞳仁正对着他。\n　　一种悚然的感觉入骨，他放入了裤兜里。\n　　陈献目前考虑的是，既然鬼物是抵抗鬼魂的唯一手段，那么他就需要更多的死魂纸来购买鬼物。\n　　还有就是，待会儿发现孙晗死亡的是他，他要不要稍微改变一下剧情？比如说，叫上男主角梁鸿一起去发现死亡的孙晗？\n　　不过，现在无论他怎么想都是无用的，要看到时的情况，总之，他想要主动地去探索这个恐怖剧本背后的真相，来获得死魂纸，从而囤积作为保命手段的鬼物。\n　　时间在一点一点的过去，轻微的鼾声逐渐响起，陈献却全然不同，一想到接下来要发生的事情，他就无法睡眠。\n　　好冷……比之前更冷了……\n　　每呼吸一次，都好像吸了一口碎冰渣似的，喉咙都被扎得生疼。\n　　陈献缩了缩身体，将自己裹得更紧些，虽然这并不能让他暖和哪怕一点。\n　　困意汹涌地袭来，眼皮像被重物压上一样越来越重，陈献慢慢失去意识，睡了过去……\n　　“吱——嘎——！”\n　　陈献睁开双眼，巨大的声响仿佛平地一声惊雷，将他猛然惊醒！\n　　旧蓝色的宿舍门竟然开了一道缝隙！\n　　黑黢黢的缝隙后好像隐藏了一只细长的眼睛，让陈献越看越心慌，如果他没记错，门本来是锁着的。\n　　外面不知何时下起了雨，打着窗棂发出“啪啪”的脆响。\n　　“扣………扣…………扣……”\n　　门被从外面敲击着，敲击声速度很慢，好像一个八十老妪在用苍老的手指一下一下敲着。\n　　“扣……扣…扣”\n　　敲击的速度明显加快了。\n　　陈献犹豫着，想来想去，还是在上铺装睡吧，他闭上了眼睛。\n　　敲门声停止了。\n　　静默又回归了寝室。\n　　突然，仿佛要把门撞碎一样，疯狂的敲门声刺入耳膜！\n　　“扣扣扣扣扣扣！！！！！！”\n　　下铺传来了缓慢的窸窸窣窣的脚步声。\n　　是谁出去了？可是先听到开门声再听到脚步声，那是有人进来了？是梁鸿吗？\n　　陈献摸到裤兜里的那个小凸起，手脚冰凉。\n　　一声凄厉的婴儿哭声在寝室内响起，近在咫尺，带着一种在孕妇羊水里的稠密。\n　　“呜哇……呜哇……”\n　　没关系的……今晚死的人是孙晗……只要他一直保持这样的状态……\n　　又没有动静了，无论是敲门声，婴儿啼哭声还是脚步声，都消失了。\n　　剧烈的尿意盈满了陈献的膀胱。\n　　他竭力控制着自己的括约肌，甚至在想，要不直接就尿在床上得了……\n　　死魂纸数量：-2（如果在剧本结束后，死魂纸数量小于-20张，则判定死亡）\n　　看来必须要走剧情了，陈献赶紧睁开眼睛坐起了身子，四周一片漆黑，伸手不见五指。\n　　过段时间后，他的眼睛适应了黑暗，稍微能看清一些东西了。\n　　他看向孙晗那边，那儿的被子里有个背对着他的人形，看不出死活。\n　　既然要发现他的死亡，只能下去了，再找个借口叫一下孙晗。\n　　陈献小心翼翼地踩着梯子下去，结果突然看到梁鸿坐在凳子上背对着他，捧着个电脑，里面放着影像。\n　　这是剧本没有的内容。\n　　“……梁鸿？你不睡觉在这里干什么？”\n　　梁鸿没有理他，只见他的电脑里放着一个宿舍的影像，里面居然出现了陈献！\n　　影像高糊，还是黑白的，但陈献还是认出了自己。\n　　在影像里，陈献被尿意憋醒下了梯子，然后看到了抱着电脑的梁鸿，接着梁鸿转过头来，由于拍摄角度原因，陈献看不到影像里的梁鸿转头后的面目，但是影像里的陈献竟然无比惊骇地尖叫一声，七窍流血地倒在了地上，肠子内脏流了一地！\n　　冷汗顺着陈献的脸
				`
			}
		}
	}
```

#跳转章节
```html
	<yingbing-ReadPage ref="page" style="height: 100vh;" @loadmore="onLoadmore"></yingbing-ReadPage>
	<button @tap="changeChapter">跳转章节</button>
```
```javascript
	export default {
		onReady () {
			let contents = [{
			    chapter: 1,//章节序号
			    content: this.getContent(1),//文本内容
			    title: '第一章',//章节标题
			    isStart: false,//是否是第一章节
			    isEnd: false,//是否是最后一个章节
			}]
			const { page } = this.$refs;
			page.init({
			    contents: contents,
			    currentChapter: 1,//从第二章节开始阅读 和章节chapter对应
				start: 0//从章节的第100个字开始阅读
			})
		},
		methods: {
			getContent (chapter = 1) {
				return `第${chapter}章
				　　灼烫或红中发黑的文字是必须执行的剧情，否则只有死路一条。\n　　作为可以交易鬼物的死魂纸，可以根据该人探知真相进度的贡献度来获得，而篡改关键性剧情，会没收一定数量的死魂纸。\n　　但这并不代表扮演者要完全忠诚地按照剧本说的去做，若果真如此，也只有死这一条路可走！相反，往往需要扮演者打破剧本中固有的剧情，才能够赢得一丝生机！\n　　页面上浮现了一行血字：\n　　【是否打开鬼物商城？】\n　　是。\n　　陈献的眼前出现了一个黑色的页面，边框熊熊燃烧着地狱之火，炙人眼球。\n　　大概二十多个鬼物被挂在页面上，几乎都是动辄需要一两千张死魂纸的鬼物。\n　　目前陈献的100张死魂纸，只够购买一个叫【死亡人眼】的鬼物。\n　　【死亡人眼】：共可使用三次，已被使用两次，剩余可击退一次轻级诅咒鬼魂的攻击，对中级诅咒鬼魂有轻微抑制性作用，对高级和顶级诅咒鬼魂无用。卖家：盲人女孩扮演者凌小薇。（在《猫脸老太太》恐怖剧本中可获得）\n　　页面的血字发生了改变。\n　　【是否使用二十张死魂纸刷新页面？】\n　　现在这个页面他能勉强买的起的就只有一个【死亡人眼】，再用20张死魂纸刷新，恐怕一个也买不了。\n　　陈献当机立断，购买了【死亡人眼】\n　　【鬼物：死亡人眼\n　　死魂纸数量：0】\n　　交易的瞬间，他的手心出现了一种圆润滑腻的触感，带着股若有若无的腥臊味。\n　　陈献打开手心一看，里面有一只玻璃球大小的眼球，上眼皮黏连在上面，很像荔枝，而眼球的瞳仁正对着他。\n　　一种悚然的感觉入骨，他放入了裤兜里。\n　　陈献目前考虑的是，既然鬼物是抵抗鬼魂的唯一手段，那么他就需要更多的死魂纸来购买鬼物。\n　　还有就是，待会儿发现孙晗死亡的是他，他要不要稍微改变一下剧情？比如说，叫上男主角梁鸿一起去发现死亡的孙晗？\n　　不过，现在无论他怎么想都是无用的，要看到时的情况，总之，他想要主动地去探索这个恐怖剧本背后的真相，来获得死魂纸，从而囤积作为保命手段的鬼物。\n　　时间在一点一点的过去，轻微的鼾声逐渐响起，陈献却全然不同，一想到接下来要发生的事情，他就无法睡眠。\n　　好冷……比之前更冷了……\n　　每呼吸一次，都好像吸了一口碎冰渣似的，喉咙都被扎得生疼。\n　　陈献缩了缩身体，将自己裹得更紧些，虽然这并不能让他暖和哪怕一点。\n　　困意汹涌地袭来，眼皮像被重物压上一样越来越重，陈献慢慢失去意识，睡了过去……\n　　“吱——嘎——！”\n　　陈献睁开双眼，巨大的声响仿佛平地一声惊雷，将他猛然惊醒！\n　　旧蓝色的宿舍门竟然开了一道缝隙！\n　　黑黢黢的缝隙后好像隐藏了一只细长的眼睛，让陈献越看越心慌，如果他没记错，门本来是锁着的。\n　　外面不知何时下起了雨，打着窗棂发出“啪啪”的脆响。\n　　“扣………扣…………扣……”\n　　门被从外面敲击着，敲击声速度很慢，好像一个八十老妪在用苍老的手指一下一下敲着。\n　　“扣……扣…扣”\n　　敲击的速度明显加快了。\n　　陈献犹豫着，想来想去，还是在上铺装睡吧，他闭上了眼睛。\n　　敲门声停止了。\n　　静默又回归了寝室。\n　　突然，仿佛要把门撞碎一样，疯狂的敲门声刺入耳膜！\n　　“扣扣扣扣扣扣！！！！！！”\n　　下铺传来了缓慢的窸窸窣窣的脚步声。\n　　是谁出去了？可是先听到开门声再听到脚步声，那是有人进来了？是梁鸿吗？\n　　陈献摸到裤兜里的那个小凸起，手脚冰凉。\n　　一声凄厉的婴儿哭声在寝室内响起，近在咫尺，带着一种在孕妇羊水里的稠密。\n　　“呜哇……呜哇……”\n　　没关系的……今晚死的人是孙晗……只要他一直保持这样的状态……\n　　又没有动静了，无论是敲门声，婴儿啼哭声还是脚步声，都消失了。\n　　剧烈的尿意盈满了陈献的膀胱。\n　　他竭力控制着自己的括约肌，甚至在想，要不直接就尿在床上得了……\n　　死魂纸数量：-2（如果在剧本结束后，死魂纸数量小于-20张，则判定死亡）\n　　看来必须要走剧情了，陈献赶紧睁开眼睛坐起了身子，四周一片漆黑，伸手不见五指。\n　　过段时间后，他的眼睛适应了黑暗，稍微能看清一些东西了。\n　　他看向孙晗那边，那儿的被子里有个背对着他的人形，看不出死活。\n　　既然要发现他的死亡，只能下去了，再找个借口叫一下孙晗。\n　　陈献小心翼翼地踩着梯子下去，结果突然看到梁鸿坐在凳子上背对着他，捧着个电脑，里面放着影像。\n　　这是剧本没有的内容。\n　　“……梁鸿？你不睡觉在这里干什么？”\n　　梁鸿没有理他，只见他的电脑里放着一个宿舍的影像，里面居然出现了陈献！\n　　影像高糊，还是黑白的，但陈献还是认出了自己。\n　　在影像里，陈献被尿意憋醒下了梯子，然后看到了抱着电脑的梁鸿，接着梁鸿转过头来，由于拍摄角度原因，陈献看不到影像里的梁鸿转头后的面目，但是影像里的陈献竟然无比惊骇地尖叫一声，七窍流血地倒在了地上，肠子内脏流了一地！\n　　冷汗顺着陈献的脸
				`
			},
			changeChapter () {
				//模拟请求
				setTimeout(() => {
				    let contents = [{
				        chapter: 4,
				        custom: this.getContent(4),
				        title: '第四章',
				        isStart: false,
				        isEnd: false
				    }]
				    const { page } = this.$refs;
				    page.change({
				        contents: contents,//携带内容跳转
				        start: 0,
				        currentChapter: 4//从第四章节开始阅读（如果插件内还未获取到该章节则会触发loadmore事件）
				    })
				}, 500)
			},
			//当跳转章节时，插件内还未获取到需要跳转的章节内容时，会触发loadmore事件来获取该章节内容
			onLoadmore (chapter, callback) {
				//模拟请求
				setTimeout(() => {
				    let content = {
				        chapter: chapter,
				        custom: this.getContent(chapter),
				        title: '第' + chapter + '章',
				        isStart: chapter == 1,
				        isEnd: chapter == 7
				    }
					callback('success', content)
					// callback('fail');
					// callback('timeout');
				}, 500)
			}
		}
	}
```

#加载更多
```html
	<yingbing-ReadPage ref="page" style="height: 100vh;" @loadmore="onLoadmore"></yingbing-ReadPage>
```
```javascript
	export default {
		onReady () {
			let contents = [{
			    chapter: 1,//章节序号
			    content: this.getContent(1),//文本内容
			    title: '第一章',//章节标题
			    isStart: false,//是否是第一章节
			    isEnd: false,//是否是最后一个章节
			}]
			const { page } = this.$refs;
			page.init({
			    contents: contents,
			    currentChapter: 1,//从第二章节开始阅读 和章节chapter对应
				start: 100//从章节的第100个字开始阅读
			})
		},
		methods: {
			getContent (chapter = 1) {
				return `第${chapter}章
				　　灼烫或红中发黑的文字是必须执行的剧情，否则只有死路一条。\n　　作为可以交易鬼物的死魂纸，可以根据该人探知真相进度的贡献度来获得，而篡改关键性剧情，会没收一定数量的死魂纸。\n　　但这并不代表扮演者要完全忠诚地按照剧本说的去做，若果真如此，也只有死这一条路可走！相反，往往需要扮演者打破剧本中固有的剧情，才能够赢得一丝生机！\n　　页面上浮现了一行血字：\n　　【是否打开鬼物商城？】\n　　是。\n　　陈献的眼前出现了一个黑色的页面，边框熊熊燃烧着地狱之火，炙人眼球。\n　　大概二十多个鬼物被挂在页面上，几乎都是动辄需要一两千张死魂纸的鬼物。\n　　目前陈献的100张死魂纸，只够购买一个叫【死亡人眼】的鬼物。\n　　【死亡人眼】：共可使用三次，已被使用两次，剩余可击退一次轻级诅咒鬼魂的攻击，对中级诅咒鬼魂有轻微抑制性作用，对高级和顶级诅咒鬼魂无用。卖家：盲人女孩扮演者凌小薇。（在《猫脸老太太》恐怖剧本中可获得）\n　　页面的血字发生了改变。\n　　【是否使用二十张死魂纸刷新页面？】\n　　现在这个页面他能勉强买的起的就只有一个【死亡人眼】，再用20张死魂纸刷新，恐怕一个也买不了。\n　　陈献当机立断，购买了【死亡人眼】\n　　【鬼物：死亡人眼\n　　死魂纸数量：0】\n　　交易的瞬间，他的手心出现了一种圆润滑腻的触感，带着股若有若无的腥臊味。\n　　陈献打开手心一看，里面有一只玻璃球大小的眼球，上眼皮黏连在上面，很像荔枝，而眼球的瞳仁正对着他。\n　　一种悚然的感觉入骨，他放入了裤兜里。\n　　陈献目前考虑的是，既然鬼物是抵抗鬼魂的唯一手段，那么他就需要更多的死魂纸来购买鬼物。\n　　还有就是，待会儿发现孙晗死亡的是他，他要不要稍微改变一下剧情？比如说，叫上男主角梁鸿一起去发现死亡的孙晗？\n　　不过，现在无论他怎么想都是无用的，要看到时的情况，总之，他想要主动地去探索这个恐怖剧本背后的真相，来获得死魂纸，从而囤积作为保命手段的鬼物。\n　　时间在一点一点的过去，轻微的鼾声逐渐响起，陈献却全然不同，一想到接下来要发生的事情，他就无法睡眠。\n　　好冷……比之前更冷了……\n　　每呼吸一次，都好像吸了一口碎冰渣似的，喉咙都被扎得生疼。\n　　陈献缩了缩身体，将自己裹得更紧些，虽然这并不能让他暖和哪怕一点。\n　　困意汹涌地袭来，眼皮像被重物压上一样越来越重，陈献慢慢失去意识，睡了过去……\n　　“吱——嘎——！”\n　　陈献睁开双眼，巨大的声响仿佛平地一声惊雷，将他猛然惊醒！\n　　旧蓝色的宿舍门竟然开了一道缝隙！\n　　黑黢黢的缝隙后好像隐藏了一只细长的眼睛，让陈献越看越心慌，如果他没记错，门本来是锁着的。\n　　外面不知何时下起了雨，打着窗棂发出“啪啪”的脆响。\n　　“扣………扣…………扣……”\n　　门被从外面敲击着，敲击声速度很慢，好像一个八十老妪在用苍老的手指一下一下敲着。\n　　“扣……扣…扣”\n　　敲击的速度明显加快了。\n　　陈献犹豫着，想来想去，还是在上铺装睡吧，他闭上了眼睛。\n　　敲门声停止了。\n　　静默又回归了寝室。\n　　突然，仿佛要把门撞碎一样，疯狂的敲门声刺入耳膜！\n　　“扣扣扣扣扣扣！！！！！！”\n　　下铺传来了缓慢的窸窸窣窣的脚步声。\n　　是谁出去了？可是先听到开门声再听到脚步声，那是有人进来了？是梁鸿吗？\n　　陈献摸到裤兜里的那个小凸起，手脚冰凉。\n　　一声凄厉的婴儿哭声在寝室内响起，近在咫尺，带着一种在孕妇羊水里的稠密。\n　　“呜哇……呜哇……”\n　　没关系的……今晚死的人是孙晗……只要他一直保持这样的状态……\n　　又没有动静了，无论是敲门声，婴儿啼哭声还是脚步声，都消失了。\n　　剧烈的尿意盈满了陈献的膀胱。\n　　他竭力控制着自己的括约肌，甚至在想，要不直接就尿在床上得了……\n　　死魂纸数量：-2（如果在剧本结束后，死魂纸数量小于-20张，则判定死亡）\n　　看来必须要走剧情了，陈献赶紧睁开眼睛坐起了身子，四周一片漆黑，伸手不见五指。\n　　过段时间后，他的眼睛适应了黑暗，稍微能看清一些东西了。\n　　他看向孙晗那边，那儿的被子里有个背对着他的人形，看不出死活。\n　　既然要发现他的死亡，只能下去了，再找个借口叫一下孙晗。\n　　陈献小心翼翼地踩着梯子下去，结果突然看到梁鸿坐在凳子上背对着他，捧着个电脑，里面放着影像。\n　　这是剧本没有的内容。\n　　“……梁鸿？你不睡觉在这里干什么？”\n　　梁鸿没有理他，只见他的电脑里放着一个宿舍的影像，里面居然出现了陈献！\n　　影像高糊，还是黑白的，但陈献还是认出了自己。\n　　在影像里，陈献被尿意憋醒下了梯子，然后看到了抱着电脑的梁鸿，接着梁鸿转过头来，由于拍摄角度原因，陈献看不到影像里的梁鸿转头后的面目，但是影像里的陈献竟然无比惊骇地尖叫一声，七窍流血地倒在了地上，肠子内脏流了一地！\n　　冷汗顺着陈献的脸
				`
			},
			onLoadmore (chapter, callback) {
				//模拟请求
				setTimeout(() => {
				    let content = {
				        chapter: chapter,
				        custom: this.getContent(chapter),
				        title: '第' + chapter + '章',
				        isStart: chapter == 1,
				        isEnd: chapter == 7
				    }
					callback('success', content)
					// callback('fail');
					// callback('timeout');
				}, 500)
			}
		}
	}
```

#预加载 （预加载不会去渲染页面，不能取代loadmore事件）
```html
	<!-- 开启预加载 -->
	<yingbing-ReadPage ref="page" style="height: 100vh;" enablePreload @preload="onPreload"></yingbing-ReadPage>
```
```javascript
	export default {
		onReady () {
			let contents = [{
			    chapter: 1,//章节序号
			    content: this.getContent(1),//文本内容
			    title: '第一章',//章节标题
			    isStart: false,//是否是第一章节
			    isEnd: false,//是否是最后一个章节
			}]
			const { page } = this.$refs;
			page.init({
			    contents: contents,
			    currentChapter: 1,//从第二章节开始阅读 和章节chapter对应
				start: 100//从章节的第100个字开始阅读
			})
		},
		methods: {
			getContent (chapter = 1) {
				return `第${chapter}章
				　　灼烫或红中发黑的文字是必须执行的剧情，否则只有死路一条。\n　　作为可以交易鬼物的死魂纸，可以根据该人探知真相进度的贡献度来获得，而篡改关键性剧情，会没收一定数量的死魂纸。\n　　但这并不代表扮演者要完全忠诚地按照剧本说的去做，若果真如此，也只有死这一条路可走！相反，往往需要扮演者打破剧本中固有的剧情，才能够赢得一丝生机！\n　　页面上浮现了一行血字：\n　　【是否打开鬼物商城？】\n　　是。\n　　陈献的眼前出现了一个黑色的页面，边框熊熊燃烧着地狱之火，炙人眼球。\n　　大概二十多个鬼物被挂在页面上，几乎都是动辄需要一两千张死魂纸的鬼物。\n　　目前陈献的100张死魂纸，只够购买一个叫【死亡人眼】的鬼物。\n　　【死亡人眼】：共可使用三次，已被使用两次，剩余可击退一次轻级诅咒鬼魂的攻击，对中级诅咒鬼魂有轻微抑制性作用，对高级和顶级诅咒鬼魂无用。卖家：盲人女孩扮演者凌小薇。（在《猫脸老太太》恐怖剧本中可获得）\n　　页面的血字发生了改变。\n　　【是否使用二十张死魂纸刷新页面？】\n　　现在这个页面他能勉强买的起的就只有一个【死亡人眼】，再用20张死魂纸刷新，恐怕一个也买不了。\n　　陈献当机立断，购买了【死亡人眼】\n　　【鬼物：死亡人眼\n　　死魂纸数量：0】\n　　交易的瞬间，他的手心出现了一种圆润滑腻的触感，带着股若有若无的腥臊味。\n　　陈献打开手心一看，里面有一只玻璃球大小的眼球，上眼皮黏连在上面，很像荔枝，而眼球的瞳仁正对着他。\n　　一种悚然的感觉入骨，他放入了裤兜里。\n　　陈献目前考虑的是，既然鬼物是抵抗鬼魂的唯一手段，那么他就需要更多的死魂纸来购买鬼物。\n　　还有就是，待会儿发现孙晗死亡的是他，他要不要稍微改变一下剧情？比如说，叫上男主角梁鸿一起去发现死亡的孙晗？\n　　不过，现在无论他怎么想都是无用的，要看到时的情况，总之，他想要主动地去探索这个恐怖剧本背后的真相，来获得死魂纸，从而囤积作为保命手段的鬼物。\n　　时间在一点一点的过去，轻微的鼾声逐渐响起，陈献却全然不同，一想到接下来要发生的事情，他就无法睡眠。\n　　好冷……比之前更冷了……\n　　每呼吸一次，都好像吸了一口碎冰渣似的，喉咙都被扎得生疼。\n　　陈献缩了缩身体，将自己裹得更紧些，虽然这并不能让他暖和哪怕一点。\n　　困意汹涌地袭来，眼皮像被重物压上一样越来越重，陈献慢慢失去意识，睡了过去……\n　　“吱——嘎——！”\n　　陈献睁开双眼，巨大的声响仿佛平地一声惊雷，将他猛然惊醒！\n　　旧蓝色的宿舍门竟然开了一道缝隙！\n　　黑黢黢的缝隙后好像隐藏了一只细长的眼睛，让陈献越看越心慌，如果他没记错，门本来是锁着的。\n　　外面不知何时下起了雨，打着窗棂发出“啪啪”的脆响。\n　　“扣………扣…………扣……”\n　　门被从外面敲击着，敲击声速度很慢，好像一个八十老妪在用苍老的手指一下一下敲着。\n　　“扣……扣…扣”\n　　敲击的速度明显加快了。\n　　陈献犹豫着，想来想去，还是在上铺装睡吧，他闭上了眼睛。\n　　敲门声停止了。\n　　静默又回归了寝室。\n　　突然，仿佛要把门撞碎一样，疯狂的敲门声刺入耳膜！\n　　“扣扣扣扣扣扣！！！！！！”\n　　下铺传来了缓慢的窸窸窣窣的脚步声。\n　　是谁出去了？可是先听到开门声再听到脚步声，那是有人进来了？是梁鸿吗？\n　　陈献摸到裤兜里的那个小凸起，手脚冰凉。\n　　一声凄厉的婴儿哭声在寝室内响起，近在咫尺，带着一种在孕妇羊水里的稠密。\n　　“呜哇……呜哇……”\n　　没关系的……今晚死的人是孙晗……只要他一直保持这样的状态……\n　　又没有动静了，无论是敲门声，婴儿啼哭声还是脚步声，都消失了。\n　　剧烈的尿意盈满了陈献的膀胱。\n　　他竭力控制着自己的括约肌，甚至在想，要不直接就尿在床上得了……\n　　死魂纸数量：-2（如果在剧本结束后，死魂纸数量小于-20张，则判定死亡）\n　　看来必须要走剧情了，陈献赶紧睁开眼睛坐起了身子，四周一片漆黑，伸手不见五指。\n　　过段时间后，他的眼睛适应了黑暗，稍微能看清一些东西了。\n　　他看向孙晗那边，那儿的被子里有个背对着他的人形，看不出死活。\n　　既然要发现他的死亡，只能下去了，再找个借口叫一下孙晗。\n　　陈献小心翼翼地踩着梯子下去，结果突然看到梁鸿坐在凳子上背对着他，捧着个电脑，里面放着影像。\n　　这是剧本没有的内容。\n　　“……梁鸿？你不睡觉在这里干什么？”\n　　梁鸿没有理他，只见他的电脑里放着一个宿舍的影像，里面居然出现了陈献！\n　　影像高糊，还是黑白的，但陈献还是认出了自己。\n　　在影像里，陈献被尿意憋醒下了梯子，然后看到了抱着电脑的梁鸿，接着梁鸿转过头来，由于拍摄角度原因，陈献看不到影像里的梁鸿转头后的面目，但是影像里的陈献竟然无比惊骇地尖叫一声，七窍流血地倒在了地上，肠子内脏流了一地！\n　　冷汗顺着陈献的脸
				`
			},
			onPreload (chapters, callback) {
				//模拟请求
				setTimeout(() => {
				   let contents = []
				   for ( let i in chapters ) {
				       contents.push({
				           chapter: chapters[i],
				           start: 0,
				           content: this.getContent(chapters[i]),
				           title: '第' + chapters[i] + '章',
				           isStart: chapters[i] == 1,
				           isEnd: chapters[i] == 7
				       })
				   }
				   callback('success', contents);
				   // callback('fail');
				   // callback('timeout');
				}, 500)
			}
		}
	}
```

#获取当前页信息
```html
	<yingbing-ReadPage style="height: 100vh;" @change="onCurrentChange"></yingbing-ReadPage>
```
```javascript
	export default {
		methods: {
			onCurrentChange (pageInfo) {
				console.log(pageInfo)//当前页信息
			}
		}
	}
```

#开启点击区域（用于唤起设置菜单等）
```html
	<yingbing-ReadPage ref="page" style="height: 100vh;" :clickOption="clickOption" enableClick @clickTo="onClickTo"></yingbing-ReadPage>
	<view v-if="menuShow">设置菜单</view>
```
```javascript
	export default {
		data () {
			return {
				menuShow: false,
				//点击事件位置设置
				clickOption: {
					width: uni.upx2px(200),
					height: uni.upx2px(200),
					left: 'auto',
					top: 'auto'
				}
			}
		},
		methods: {
			onClickTo () {
				this.menuShow = true
			}
		}
	}
```

#JS控制翻页
```html
	<yingbing-ReadPage ref="page" style="height: 100vh;"></yingbing-ReadPage>
	<button @tap="pagePrev">向前翻页</button>
	<button @tap="pageNext">向后翻页</button>
```
```javascript
	export default {
		methods: {
			pageNext () {
				//滚动模式下表现为向下滚动一小段距离
				this.$refs.page.pageNext()
			},
			pagePrev () {
				//滚动模式下表现为向上滚动一小段距离
				this.$refs.page.pagePrev()
			}
		}
	}
```

#自动翻页
```html
	<!-- 开启自动翻页 -->
	<yingbing-ReadPage ref="page" style="height: 100vh;" autoplay></yingbing-ReadPage>
```
```javascript
	export default {
		onReady () {
			let contents = [{
			    chapter: 1,//章节序号
			    content: this.getContent(1),//文本内容
			    title: '第一章',//章节标题
			    isStart: false,//是否是第一章节
			    isEnd: false,//是否是最后一个章节
			}]
			const { page } = this.$refs;
			page.init({
			    contents: contents,
			    currentChapter: 1,//从第二章节开始阅读 和章节chapter对应
				start: 100//从章节的第100个字开始阅读
			})
		},
		methods: {
			getContent (chapter = 1) {
				return `第${chapter}章
				　　灼烫或红中发黑的文字是必须执行的剧情，否则只有死路一条。\n　　作为可以交易鬼物的死魂纸，可以根据该人探知真相进度的贡献度来获得，而篡改关键性剧情，会没收一定数量的死魂纸。\n　　但这并不代表扮演者要完全忠诚地按照剧本说的去做，若果真如此，也只有死这一条路可走！相反，往往需要扮演者打破剧本中固有的剧情，才能够赢得一丝生机！\n　　页面上浮现了一行血字：\n　　【是否打开鬼物商城？】\n　　是。\n　　陈献的眼前出现了一个黑色的页面，边框熊熊燃烧着地狱之火，炙人眼球。\n　　大概二十多个鬼物被挂在页面上，几乎都是动辄需要一两千张死魂纸的鬼物。\n　　目前陈献的100张死魂纸，只够购买一个叫【死亡人眼】的鬼物。\n　　【死亡人眼】：共可使用三次，已被使用两次，剩余可击退一次轻级诅咒鬼魂的攻击，对中级诅咒鬼魂有轻微抑制性作用，对高级和顶级诅咒鬼魂无用。卖家：盲人女孩扮演者凌小薇。（在《猫脸老太太》恐怖剧本中可获得）\n　　页面的血字发生了改变。\n　　【是否使用二十张死魂纸刷新页面？】\n　　现在这个页面他能勉强买的起的就只有一个【死亡人眼】，再用20张死魂纸刷新，恐怕一个也买不了。\n　　陈献当机立断，购买了【死亡人眼】\n　　【鬼物：死亡人眼\n　　死魂纸数量：0】\n　　交易的瞬间，他的手心出现了一种圆润滑腻的触感，带着股若有若无的腥臊味。\n　　陈献打开手心一看，里面有一只玻璃球大小的眼球，上眼皮黏连在上面，很像荔枝，而眼球的瞳仁正对着他。\n　　一种悚然的感觉入骨，他放入了裤兜里。\n　　陈献目前考虑的是，既然鬼物是抵抗鬼魂的唯一手段，那么他就需要更多的死魂纸来购买鬼物。\n　　还有就是，待会儿发现孙晗死亡的是他，他要不要稍微改变一下剧情？比如说，叫上男主角梁鸿一起去发现死亡的孙晗？\n　　不过，现在无论他怎么想都是无用的，要看到时的情况，总之，他想要主动地去探索这个恐怖剧本背后的真相，来获得死魂纸，从而囤积作为保命手段的鬼物。\n　　时间在一点一点的过去，轻微的鼾声逐渐响起，陈献却全然不同，一想到接下来要发生的事情，他就无法睡眠。\n　　好冷……比之前更冷了……\n　　每呼吸一次，都好像吸了一口碎冰渣似的，喉咙都被扎得生疼。\n　　陈献缩了缩身体，将自己裹得更紧些，虽然这并不能让他暖和哪怕一点。\n　　困意汹涌地袭来，眼皮像被重物压上一样越来越重，陈献慢慢失去意识，睡了过去……\n　　“吱——嘎——！”\n　　陈献睁开双眼，巨大的声响仿佛平地一声惊雷，将他猛然惊醒！\n　　旧蓝色的宿舍门竟然开了一道缝隙！\n　　黑黢黢的缝隙后好像隐藏了一只细长的眼睛，让陈献越看越心慌，如果他没记错，门本来是锁着的。\n　　外面不知何时下起了雨，打着窗棂发出“啪啪”的脆响。\n　　“扣………扣…………扣……”\n　　门被从外面敲击着，敲击声速度很慢，好像一个八十老妪在用苍老的手指一下一下敲着。\n　　“扣……扣…扣”\n　　敲击的速度明显加快了。\n　　陈献犹豫着，想来想去，还是在上铺装睡吧，他闭上了眼睛。\n　　敲门声停止了。\n　　静默又回归了寝室。\n　　突然，仿佛要把门撞碎一样，疯狂的敲门声刺入耳膜！\n　　“扣扣扣扣扣扣！！！！！！”\n　　下铺传来了缓慢的窸窸窣窣的脚步声。\n　　是谁出去了？可是先听到开门声再听到脚步声，那是有人进来了？是梁鸿吗？\n　　陈献摸到裤兜里的那个小凸起，手脚冰凉。\n　　一声凄厉的婴儿哭声在寝室内响起，近在咫尺，带着一种在孕妇羊水里的稠密。\n　　“呜哇……呜哇……”\n　　没关系的……今晚死的人是孙晗……只要他一直保持这样的状态……\n　　又没有动静了，无论是敲门声，婴儿啼哭声还是脚步声，都消失了。\n　　剧烈的尿意盈满了陈献的膀胱。\n　　他竭力控制着自己的括约肌，甚至在想，要不直接就尿在床上得了……\n　　死魂纸数量：-2（如果在剧本结束后，死魂纸数量小于-20张，则判定死亡）\n　　看来必须要走剧情了，陈献赶紧睁开眼睛坐起了身子，四周一片漆黑，伸手不见五指。\n　　过段时间后，他的眼睛适应了黑暗，稍微能看清一些东西了。\n　　他看向孙晗那边，那儿的被子里有个背对着他的人形，看不出死活。\n　　既然要发现他的死亡，只能下去了，再找个借口叫一下孙晗。\n　　陈献小心翼翼地踩着梯子下去，结果突然看到梁鸿坐在凳子上背对着他，捧着个电脑，里面放着影像。\n　　这是剧本没有的内容。\n　　“……梁鸿？你不睡觉在这里干什么？”\n　　梁鸿没有理他，只见他的电脑里放着一个宿舍的影像，里面居然出现了陈献！\n　　影像高糊，还是黑白的，但陈献还是认出了自己。\n　　在影像里，陈献被尿意憋醒下了梯子，然后看到了抱着电脑的梁鸿，接着梁鸿转过头来，由于拍摄角度原因，陈献看不到影像里的梁鸿转头后的面目，但是影像里的陈献竟然无比惊骇地尖叫一声，七窍流血地倒在了地上，肠子内脏流了一地！\n　　冷汗顺着陈献的脸
				`
			}
		}
	}
```

#设置翻页
```html
	<yingbing-ReadPage ref="page" :pageType="pageType" style="height: 100vh;"></yingbing-ReadPage>
	<button @tap="changePageType('real')">仿真翻页</button>
	<button @tap="changePageType('cover')">覆盖翻页</button>
	<button @tap="changePageType('none')">无翻页效果</button>
	<button @tap="changePageType('scroll')">滚动</button>
```
```javascript
	export default {
		data () {
			return {
				pageType: 'cover'
			}
		},
		methods: {
			changePageType (type) {
				this.pageType = type
			}
		}
	}
```

#设置自定义字体
```html
	<yingbing-ReadPage ref="page" :fontFamily="fontFamily" :fontFace="fontFace" style="height: 100vh;"></yingbing-ReadPage>
	<button @tap="changeFontFamily">切换字体</button>
```

* 切换字体
```javascript
	export default {
		data () {
			return {
				fontFamily: 'customfontfamily'
			}
		},
		methods: {
			changeFontFamily () {
				this.fontFamily = this.fontFamily == 'customfontfamily' ? 'customfontfamily2' : 'customfontfamily'
			}
		}
	}
```

* 加载本地字体文件路径（仅支持H5和APP-VUE）
```javascript
	export default {
		data () {
			return {
				// #ifdef APP-VUE || H5
				fontFace: [{
					fontFamily: 'customfontfamily',//字体名称
					src: './static/font.ttf'//本地字体文件路径
				},{
					fontFamily: 'customfontfamily2',//字体名称
					src: './static/font2.ttf'//本地字体文件路径
				}]//第三方字体
				// #endif
			}
		}
	}
```

* 加载网络字体文件路径 (微信小程序只支持https，h5不能跨域)
```javascript
	export default {
		data () {
			return {
				fontFace: [{
					fontFamily: 'customfontfamily',//字体名称
					src: 'https://www.test.com/static/font.ttf'//网络字体文件路径
				}]//第三方字体
			}
		}
	}
```

* 加载base64字体 (全端支持)
```javascript
	export default {
		data () {
			return {
				fontFace: [{
					fontFamily: 'customfontfamily',//字体名称
					src: 'data:application/octet-stream;base64,AAEAAAAKAIAAA......'//将ttf文件转为base64
				}]//第三方字体
			}
		}
	}
```

#整书模式
```html
	<!-- 开启整书模式 传入一整本小说 -->
	<yingbing-ReadPage noChapter @setCatalog="setCatalog" ref="page" style="height: 100vh;"></yingbing-ReadPage>
	<button @tap="changeChapter">跳转第二章节</button>
```
```javascript
	export default {
		data () {
			catalog: []
		},
		onReady () {
			let content = this.getContent(1) + this.getContent(2) + this.getContent(3)
			page.init({
			    content: content,
				title: '测试小说',
				currentChapter: 2,
				start: 0
			})
		},
		methods: {
			getContent (chapter = 1) {
				return `第${chapter}章
				　　灼烫或红中发黑的文字是必须执行的剧情，否则只有死路一条。\n　　作为可以交易鬼物的死魂纸，可以根据该人探知真相进度的贡献度来获得，而篡改关键性剧情，会没收一定数量的死魂纸。\n　　但这并不代表扮演者要完全忠诚地按照剧本说的去做，若果真如此，也只有死这一条路可走！相反，往往需要扮演者打破剧本中固有的剧情，才能够赢得一丝生机！\n　　页面上浮现了一行血字：\n　　【是否打开鬼物商城？】\n　　是。\n　　陈献的眼前出现了一个黑色的页面，边框熊熊燃烧着地狱之火，炙人眼球。\n　　大概二十多个鬼物被挂在页面上，几乎都是动辄需要一两千张死魂纸的鬼物。\n　　目前陈献的100张死魂纸，只够购买一个叫【死亡人眼】的鬼物。\n　　【死亡人眼】：共可使用三次，已被使用两次，剩余可击退一次轻级诅咒鬼魂的攻击，对中级诅咒鬼魂有轻微抑制性作用，对高级和顶级诅咒鬼魂无用。卖家：盲人女孩扮演者凌小薇。（在《猫脸老太太》恐怖剧本中可获得）\n　　页面的血字发生了改变。\n　　【是否使用二十张死魂纸刷新页面？】\n　　现在这个页面他能勉强买的起的就只有一个【死亡人眼】，再用20张死魂纸刷新，恐怕一个也买不了。\n　　陈献当机立断，购买了【死亡人眼】\n　　【鬼物：死亡人眼\n　　死魂纸数量：0】\n　　交易的瞬间，他的手心出现了一种圆润滑腻的触感，带着股若有若无的腥臊味。\n　　陈献打开手心一看，里面有一只玻璃球大小的眼球，上眼皮黏连在上面，很像荔枝，而眼球的瞳仁正对着他。\n　　一种悚然的感觉入骨，他放入了裤兜里。\n　　陈献目前考虑的是，既然鬼物是抵抗鬼魂的唯一手段，那么他就需要更多的死魂纸来购买鬼物。\n　　还有就是，待会儿发现孙晗死亡的是他，他要不要稍微改变一下剧情？比如说，叫上男主角梁鸿一起去发现死亡的孙晗？\n　　不过，现在无论他怎么想都是无用的，要看到时的情况，总之，他想要主动地去探索这个恐怖剧本背后的真相，来获得死魂纸，从而囤积作为保命手段的鬼物。\n　　时间在一点一点的过去，轻微的鼾声逐渐响起，陈献却全然不同，一想到接下来要发生的事情，他就无法睡眠。\n　　好冷……比之前更冷了……\n　　每呼吸一次，都好像吸了一口碎冰渣似的，喉咙都被扎得生疼。\n　　陈献缩了缩身体，将自己裹得更紧些，虽然这并不能让他暖和哪怕一点。\n　　困意汹涌地袭来，眼皮像被重物压上一样越来越重，陈献慢慢失去意识，睡了过去……\n　　“吱——嘎——！”\n　　陈献睁开双眼，巨大的声响仿佛平地一声惊雷，将他猛然惊醒！\n　　旧蓝色的宿舍门竟然开了一道缝隙！\n　　黑黢黢的缝隙后好像隐藏了一只细长的眼睛，让陈献越看越心慌，如果他没记错，门本来是锁着的。\n　　外面不知何时下起了雨，打着窗棂发出“啪啪”的脆响。\n　　“扣………扣…………扣……”\n　　门被从外面敲击着，敲击声速度很慢，好像一个八十老妪在用苍老的手指一下一下敲着。\n　　“扣……扣…扣”\n　　敲击的速度明显加快了。\n　　陈献犹豫着，想来想去，还是在上铺装睡吧，他闭上了眼睛。\n　　敲门声停止了。\n　　静默又回归了寝室。\n　　突然，仿佛要把门撞碎一样，疯狂的敲门声刺入耳膜！\n　　“扣扣扣扣扣扣！！！！！！”\n　　下铺传来了缓慢的窸窸窣窣的脚步声。\n　　是谁出去了？可是先听到开门声再听到脚步声，那是有人进来了？是梁鸿吗？\n　　陈献摸到裤兜里的那个小凸起，手脚冰凉。\n　　一声凄厉的婴儿哭声在寝室内响起，近在咫尺，带着一种在孕妇羊水里的稠密。\n　　“呜哇……呜哇……”\n　　没关系的……今晚死的人是孙晗……只要他一直保持这样的状态……\n　　又没有动静了，无论是敲门声，婴儿啼哭声还是脚步声，都消失了。\n　　剧烈的尿意盈满了陈献的膀胱。\n　　他竭力控制着自己的括约肌，甚至在想，要不直接就尿在床上得了……\n　　死魂纸数量：-2（如果在剧本结束后，死魂纸数量小于-20张，则判定死亡）\n　　看来必须要走剧情了，陈献赶紧睁开眼睛坐起了身子，四周一片漆黑，伸手不见五指。\n　　过段时间后，他的眼睛适应了黑暗，稍微能看清一些东西了。\n　　他看向孙晗那边，那儿的被子里有个背对着他的人形，看不出死活。\n　　既然要发现他的死亡，只能下去了，再找个借口叫一下孙晗。\n　　陈献小心翼翼地踩着梯子下去，结果突然看到梁鸿坐在凳子上背对着他，捧着个电脑，里面放着影像。\n　　这是剧本没有的内容。\n　　“……梁鸿？你不睡觉在这里干什么？”\n　　梁鸿没有理他，只见他的电脑里放着一个宿舍的影像，里面居然出现了陈献！\n　　影像高糊，还是黑白的，但陈献还是认出了自己。\n　　在影像里，陈献被尿意憋醒下了梯子，然后看到了抱着电脑的梁鸿，接着梁鸿转过头来，由于拍摄角度原因，陈献看不到影像里的梁鸿转头后的面目，但是影像里的陈献竟然无比惊骇地尖叫一声，七窍流血地倒在了地上，肠子内脏流了一地！\n　　冷汗顺着陈献的脸
				`
			},
			//记录插件计算出的章节列表，后面的操作如跳转都以此章节列表为基础
			setCatalog (catalog) {
				this.catalog = catalog
			},
			changeChapter (chapter) {
				const { page } = this.$refs
				page.change({
				    currentChapter: 2,
					start: 0
				})
			}
		}
	}
```

#自定义页面
* 自定义页面可用于
	- 广告展示
	- 插图展示
	- 付费章节
	- ...
* 自定义页面只能放入章节最后展示
* 自定义页面有图片或者不定宽高的元素时，最好将高度定好，避免滚动模式下定位异常
* 自定义页面只能使用html的标签，不能使用uni-app的标签，不然app端会有问题
* 自定义页面的支持自定义点击事件有 APP-VUE 、H5、APP-NVUE
* 自定义页面在微信小程序 使用rich-text实现，所以部分html元素并不支持

* 如何添加自定义页面
```html
	<yingbing-ReadPage ref="page" style="height:100vh"></yingbing-ReadPage>
```
```javascript
	export default {
		onReady () {
			let contents = [{
				chapter: 1,
				content: '',//如果该章节只展示自定义页面，则文本内容可为空或不填,适用于付费章节展示
				custom: [
					`<div>自定义页面第一页</div>`
					`<div>自定义页面第二页</div>`
				],//自定义页面
				title: '第一章',
				isStart: false,
				isEnd: false
			}]
			const { page } = this.$refs
			page.init({
			    contents: contents,
			    currentChapter: 1,
				start: 0
			})
		}
	}
```

* 如何添加带点击事件的自定义页面（微信小程序不支持）
```html
	<yingbing-ReadPage ref="page" style="height:100vh" @clickme="clickme" @clickher="clickher"></yingbing-ReadPage>
```
```javascript
	export default {
		onReady () {
			let contents = [{
				chapter: 1,
				content: '',//如果该章节只展示自定义页面，则文本内容可为空或不填,适用于付费章节展示
				custom: [
					`<div onclick="clickme">自定义页面第一页</div>`
					`<div onclick="clickher(123)">自定义页面第二页</div>`
				],//自定义页面
				title: '第一章',
				isStart: false,
				isEnd: false
			}]
			const { page } = this.$refs
			page.init({
			    contents: contents,
			    currentChapter: 1,
				start: 0
			})
		},
		methods: {
			clickme () {
				console.log('点击了我')
			},
			clickher (arg) {
				console.log('点击了她', arg)
			}
		}
	}
```

#自定义插槽 （如果想实现多平台，建议使用自定义插槽替代自定义页面）
* 对于自定义页面不支持的uni-app的组件，可以使用自定义插槽实现
* 自定义插槽的功能和自定义页面类似

* 如何添加自定义插槽（微信小程序不支持这种写法）
```html
	<yingbing-ReadPage ref="page">
		<template #test>
			<text>测试插槽</text>
		</template>
	</yingbing-ReadPage>
	<yingbing-ReadPage ref="page">
		<!-- 作用域插槽（动态绑定插槽内容） -->
		<template v-slot:test={prop}>
			<text>测试插槽{{prop.chapter}}</text>
		</template>
	</yingbing-ReadPage>
```
```javascript
	export default {
		onReady () {
			let contents = [{
				chapter: 1,
				custom: [
					`slot:test`
				],//自定义页面
				title: '第一章',
				isStart: false,
				isEnd: false,
			},{
				chapter: 2,
				custom: [
					`slot:test`
				],//自定义页面
				title: '第二章',
				isStart: false,
				isEnd: false,
			}]
			const { page } = this.$refs
			page.init({
			    contents: contents,
			    currentChapter: 1,
				start: 0
			})
		}
	}
```

* 如何添加自定义插槽（兼容多端）
```html
	<yingbing-ReadPage ref="page">
		<!-- #ifndef APP-VUE -->
		<!-- APP-VUE不能够用遍历对象的属性来动态绑定插槽名称 -->
		<template v-for="(item, index) in contents" :slot="'test'+item.chapter">
			<text>测试插槽{{item.chapter}}</text>
		</template>
		<!-- #endif -->
		<!-- #ifdef APP-VUE -->
		<!-- APP-VUE只能够用index索引来动态绑定插槽名称 -->
		<template v-for="(item, index) in contents" :slot="'test'+(index+1)">
			<text>测试插槽{{item.chapter}}</text>
		</template>
		<!-- #endif -->
	</yingbing-ReadPage>
```
```javascript
	export default {
		data () {
			return {
				contents: []
			}
		},
		onReady () {
			this.contents = [{
				chapter: 1,
				custom: [
					`slot:test1`
				],//自定义页面
				title: '第一章',
				isStart: false,
				isEnd: false,
			},{
				chapter: 2,
				custom: [
					`slot:test2`
				],//自定义页面
				title: '第二章',
				isStart: false,
				isEnd: false,
			}]
			const { page } = this.$refs
			page.init({
			    contents: contents,
			    currentChapter: 1,
				start: 0
			})
		}
	}
```