let stopInterval = false
let touchtime = 0

const windowWidth = uni.getSystemInfoSync().screenWidth
const windowHeight = uni.getSystemInfoSync().screenHeight

function create ({ width, height, left, top, text, border, touch, func, dark }) {
	const btn = new plus.nativeObj.View('overall-btn', {
		width: width + 'px',
		height: height + 'px',
		top: top + 'px',
		left: left + 'px'
	});
	drawRect(btn, width, height, border || 5, dark || false)
	if ( text ) {
		drawText(btn, width, height, text, dark || false)
	}
	const setInterval = function () {
		setTimeout(() => {
			touchtime += 50
			if ( !stopInterval ) {
				setInterval()
			}
		}, 50)
	}
	if ( touch ) {
		btn.addEventListener("touchstart", (e) => {
			stopInterval = false
			setInterval()
		});
		btn.addEventListener("touchmove", (e) => {
			top = e.pageY - (height / 2)
			left = e.pageX - (width / 2)
			if ( top < 0 ) top = 0
			if ( top > windowHeight - height ) top = windowHeight - height
			if ( left < 0 ) left = 0
			if ( left > windowWidth - width ) left = windowWidth - width
			btn.setStyle({top: top + 'px', left: left + 'px'})
		});
		btn.addEventListener("touchend", (e) => {
			stopInterval = true
			if ( touchtime < 200 && func ) {
				e.top = top
				e.left = left
				func(e)
			}
			touchtime = 0
		});
	} else {
		if ( func ) {
			btn.addEventListener("click", (e) => {
				func(e)
			});
		}
	}
	return {
		show: () => {
			return btn.show()
		},
		hide: () => {
			return btn.hide()
		},
		close: () => {
			return btn.close()
		},
		draw: (text) => {
			drawText(btn, width, height, text, dark || false)
		},
		reset: ({dark}) => {
			drawRect(btn, width, height, border, dark || false)
		}
	}
}

function drawRect ( btn, width, height, border, dark ) {
	const opacity = 0.3
	const bgColor = dark ? `rgba(255,255,255,${opacity})` : `rgba(0,0,0,${opacity})`
	const borderColor = dark ? `rgba(50,50,50,${opacity})` : `rgba(225,225,225,${opacity})`
	btn.draw([{
		tag:'rect',
		id:'bg',
		rectStyles: {
			color: bgColor,
			radius: height + 'px',
			borderColor: borderColor,
			borderWidth: border + 'px',
		},
		position: {
			top: (border / 2) + 'px',
			left: (border / 2) + 'px',
			width: (width - border) + 'px',
			height: (height - border) + 'px',
		}
	}])
}

function drawText ( btn, width, height, text, dark ) {
	const color = dark ? 'rgba(0,0,0,0.7)' : 'rgba(255,255,255,0.7)'
	btn.draw([{
		tag:'font', id:'text', text: text,
		textStyles: {
			size: (height / 2) + 'px',
			color: color,
			verticalAlign: 'middle',
			weight: 'bold',
			align: 'center'
		}
	}])
}

export default create