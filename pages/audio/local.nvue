<template>
	<yb-page allowScroll :style="{'background-color': skinColor.color_bg_1}">
		<yb-nav-bar :fixed="fixed" :options="{
			bgColor: skinColor.color_theme_1,
			color: skinColor.color_4
		}" title="本地音频"></yb-nav-bar>
		<yb-file-list
		:path="path"
		@pathChange="pathChange"
		@item-click="itemClick"
		@items-click="itemsClick"
		:options="{
			bgColor: skinColor.color_bg_1,
			titleColor: skinColor.color_1,
			textColor: skinColor.color_3,
			gapColor: skinColor.color_gap_1
		}"
		suffix="flac,m4a,ogg,ape,amr,wma,wav,mp3,aac"
		:itemsMenus="itemsMenus">
			<template #flac>
				<rd-icon name="music-circle-fill" :size="75" :color="skinColor.color_orange"></rd-icon>
			</template>
			<template #m4a>
				<rd-icon name="music-circle-fill" :size="75" :color="skinColor.color_orange"></rd-icon>
			</template>
			<template #ogg>
				<rd-icon name="music-circle-fill" :size="75" :color="skinColor.color_orange"></rd-icon>
			</template>
			<template #ape>
				<rd-icon name="music-circle-fill" :size="75" :color="skinColor.color_orange"></rd-icon>
			</template>
			<template #amr>
				<rd-icon name="music-circle-fill" :size="75" :color="skinColor.color_orange"></rd-icon>
			</template>
			<template #wma>
				<rd-icon name="music-circle-fill" :size="75" :color="skinColor.color_orange"></rd-icon>
			</template>
			<template #wav>
				<rd-icon name="music-circle-fill" :size="75" :color="skinColor.color_orange"></rd-icon>
			</template>
			<template #mp3>
				<rd-icon name="music-circle-fill" :size="75" :color="skinColor.color_orange"></rd-icon>
			</template>
			<template #aac>
				<rd-icon name="music-circle-fill" :size="75" :color="skinColor.color_orange"></rd-icon>
			</template>
		</yb-file-list>
		<yb-notify ref="notify"></yb-notify>
		<yb-dialog ref="dialog"></yb-dialog>
	</yb-page>
</template>

<script>
	import appMixin from '@/common/mixin/app.js'
	import Single from '@/assets/constructor/single.js'
	import Gather from '@/assets/constructor/single.js'
	export default {
		mixins: [appMixin],
		data () {
			return {
				itemsMenus: [{
					label: '收藏音频',
					success: (list) => {
						this.$refs.dialog.prompt({
							title: {
								color: this.skinColor.color_1,
								text: '指定音频类型'
							},
							form: [{
								type: 'radio',
								required: true,
								row: true,
								wrap: true,
								radios: Object.keys(this.app.$config.audioTypes.single).map(key => {
									return {
										label: {
											text: this.app.$config.audioTypes.single[key],
											color: this.skinColor.color_3
										},
										value: key,
										options: {
											shape: 'square',
											color: this.skinColor.color_3,
											bgColor: this.skinColor.color_bg_1,
											focus: this.skinColor.color_actived_1
										}
									}
								})
							}],
							options: {
								bgColor: this.skinColor.color_bg_2
							},
							success: res => {
								if ( res.confirm && res.form[0].value ) {
									let songs = list.map(item => {
										return new Single({
											id: item.path + '_local',
											title: getApp().globalData.$utils.removeSuffix(item.name),
											src: item.path,
											style: '本地音乐',
											type: res.form[0].value,
											source: 'local'
										})
									})
									this.app.$business.toCollection(songs)
								}
							}
						})
					}
				}]
			}
		},
		computed: {
			path () {
				return this.$store.getters['path/get'].find(path => path.type == 'music')?.src
			}
		},
		methods: {
			pathChange (src) {
				this.$store.dispatch('path/add', {
					src: src,
					type: 'music'
				})
			},
			itemClick (params) {
				this.$refs.dialog.prompt({
					title: {
						color: this.skinColor.color_1,
						text: '指定音频类型'
					},
					form: [{
						type: 'radio',
						required: true,
						row: true,
						wrap: true,
						radios: Object.keys(this.app.$config.audioTypes.single).map(key => {
							return {
								label: {
									text: this.app.$config.audioTypes.single[key],
									color: this.skinColor.color_3
								},
								value: key,
								options: {
									shape: 'square',
									color: this.skinColor.color_3,
									bgColor: this.skinColor.color_bg_1,
									focus: this.skinColor.color_actived_1
								}
							}
						})
					}],
					options: {
						bgColor: this.skinColor.color_bg_2
					},
					success: res => {
						if ( res.confirm && res.form[0].value ) {
							this.app.$business.openDetail(
								new Single({
									id: params.path + '_local',
									title: getApp().globalData.$utils.removeSuffix(params.name),
									src: params.path,
									type: res.form[0].value,
									source: 'local'
								})
							)
						}
					}
				})
			},
			itemsClick (list) {
				if ( list.length < 2 ) {
					this.itemClick(list[0])
					return
				}
				this.$refs.dialog.confirm({
					title: {
						color: this.skinColor.color_1,
						text: '提示'
					},
					content: {
						color: this.skinColor.color_2,
						text: '创建一个音频合集？点击确认创建继续'
					},
					options: {
						bgColor: this.skinColor.color_bg_2,
						type: 2
					},
					confirm: '确认创建',
					cancel: '直接播放',
					success: res => {
						if ( res.confirm ) {
							let form = [{
								placeholder: '请输入合集名称',
								required: true,
								options: {
									color: this.skinColor.color_1
								}
							},{
								type: 'radio',
								required: true,
								row: true,
								wrap: true,
								radios: Object.keys(this.app.$config.audioTypes.gather).map(key => {
									return {
										label: {
											text: this.app.$config.audioTypes.gather[key],
											color: this.skinColor.color_3
										},
										value: key,
										options: {
											shape: 'square',
											color: this.skinColor.color_3,
											bgColor: this.skinColor.color_bg_1,
											focus: this.skinColor.color_actived_1
										}
									}
								})
							}]
							if ( this.$store.getters['app/getAdult'] ) {
								form.push({
									type: 'checkbox',
									checkboxs: [{
										label: {
											text: '是否敏感内容',
											color: this.skinColor.color_3
										},
										value: true,
										options: {
											shape: 'square',
											color: this.skinColor.color_3,
											bgColor: this.skinColor.color_bg_1,
											focus: this.skinColor.color_actived_1
										}
									}]
								})
							}
							this.$refs.dialog.prompt({
								title: {
									color: this.skinColor.color_1,
									text: '新建音频合集'
								},
								form: form,
								options: {
									bgColor: this.skinColor.color_bg_2
								},
								success: res => {
									if ( res.confirm ) {
										let id = new Date().getTime().toString() + Math.floor(Math.random() * Math.pow(10, 5)) + '_local'
										let index = this.$store.getters['collection/get'].findIndex(item => item.id == id)
										if ( index > -1 ) {
											this.$refs.notify.warning('随机ID重复，请再次创建')
											return
										}
										let relation = Object.keys(this.app.$config.relation)
										let relationIndex = relation.findIndex(key => this.app.$config.relation[key].gather.split(',').map(t => t.trim()).indexOf(res.form[1].value) > -1)
										let type = relationIndex > -1 ? this.app.$config.relation[relation[relationIndex]].single : 'song'
										let songs = list.map(item => {
											return new Single({
												id: item.path + '_local',
												title: getApp().globalData.$utils.removeSuffix(item.name),
												src: item.path,
												type: type,
												source: 'local'
											})
										})
										let gather = new Gather({
											id: id,
											title: res.form[0].value,
											creator: this.app.$config.app.name,
											avatar: this.app.$config.app.logo,
											list: songs,
											type: res.form[1].value,
											isAdult: this.$store.getters['app/getAdult'] ? res.form[2].value && res.form[2].value[0] ? true : false : false,
											source: 'local'
										})
										this.app.$business.openDetail(gather)
									}
								}
							})
						} else {
							this.$refs.dialog.prompt({
								title: {
									color: this.skinColor.color_1,
									text: '指定音频类型'
								},
								form: [{
									type: 'radio',
									required: true,
									row: true,
									wrap: true,
									radios: Object.keys(this.app.$config.audioTypes.single).map(key => {
										return {
											label: {
												text: this.app.$config.audioTypes.single[key],
												color: this.skinColor.color_3
											},
											value: key,
											options: {
												shape: 'square',
												color: this.skinColor.color_3,
												bgColor: this.skinColor.color_bg_1,
												focus: this.skinColor.color_actived_1
											}
										}
									})
								}],
								options: {
									bgColor: this.skinColor.color_bg_2
								},
								success: res => {
									if ( res.confirm && res.form[0].value ) {
										let songs = list.map(item => {
											return new Single({
												id: item.path + '_local',
												title: getApp().globalData.$utils.removeSuffix(item.name),
												src: item.path,
												type: res.form[0].value,
												source: 'local'
											})
										})
										this.$store.dispatch('audio/addSong', songs)
										uni.$emit('goPlayer')
									}
								}
							})
						}
					}
				})
			}
		}
	}
</script>

<style>
</style>
