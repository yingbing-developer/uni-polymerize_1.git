<template>
	<yb-page>
		<yb-tab-list
		ref="tabs"
		@change="onCurrentChange"
		:data="tabs"
		:options="options">
			<template #right>
				<view class="tabs-filters yb-flex yb-align-end yb-justify-between" @tap="selectType(current)">
					<view class="tabs-filter tabs-filter_1" :style="{
						'background-color': skinColor.color_2
					}"></view>
					<view class="tabs-filter tabs-filter_2" :style="{
						'background-color': skinColor.color_2
					}"></view>
					<view class="tabs-filter tabs-filter_3" :style="{
						'background-color': skinColor.color_2
					}"></view>
				</view>
			</template>
			<template #top>
				<yb-nav-bar
				@inputClick="app.$Router.push({
					path: '/pages/search/index'
				})"
				tabbar
				:input="{
					show: true,
					color: skinColor.color_2,
					bgColor: skinColor.color_gap_1
				}"
				@buttonClick="onButtonClick"
				:buttons="[{
					value: 'collection',
					right: 'right'
				},{
					value: 'footmark',
					right: 'right'
				},{
					value: 'partition',
					right: 'right'
				}]"
				:options="{
					color: skinColor.color_1,
					bgColor: skinColor.color_bg_1,
					statusBarColor: 'rgba(0,0,0,.4)'
				}">
					<template #collection>
						<rd-icon class="icon" name="collection" :color="skinColor.color_1" size="45"></rd-icon>
					</template>
					<template #footmark>
						<rd-icon class="icon" name="record" weight="bold" :color="skinColor.color_1" size="40"></rd-icon>
					</template>
					<template #partition>
						<rd-icon class="icon" name="type" weight="bold" :color="skinColor.color_1" size="40"></rd-icon>
					</template>
				</yb-nav-bar>
			</template>
			<template v-for="(tab, i) in tabs" :slot="i">
				<rd-gathers
				:list="data[i].list"
				ref="list"
				:index="i"
				pulldown
				:empty="data[i].list.length == 0 && data[i].isLastPage"
				:loading="data[i].loading"
				loadmore
				@loadmore="onLoadmore"
				@pulldown="onPulldown">
				</rd-gathers>
			</template>
		</yb-tab-list>
		<music-audio></music-audio>
		<yb-dialog ref="dialog"></yb-dialog>
	</yb-page>
</template>

<script>
	import appMixin from '@/common/mixin/app.js'
	export default {
		mixins: [appMixin],
		computed: {
			options () {
				return {
					bgColor: this.skinColor.color_bg_1,
					color: this.skinColor.color_1,
					gapColor: this.skinColor.color_gap_1,
					itemColor: this.skinColor.color_gap_2,
					sliderColor: this.skinColor.color_actived_1,
					focusColor: this.skinColor.color_blue,
					column: this.tabs.length > 6 ? 'auto' : this.tabs.length
				}
			}
		},
		data () {
			return {
				tabs: [],
				data: [],
				current: 0
			}
		},
		onShow() {
			this.app.$business.setSkinColor()
		},
		onResize() {
			this.$nextTick(function () {
				this.$refs.tabs.refresh()
			})
		},
		onReady() {
			this.$store.dispatch('cache/clear', 'image')
			this.tabs = Object.keys(this.app.$config.conTypes).map(key => {
				return {
					label: this.app.$config.conTypes[key],
					value: key,
				}
			})
			this.data = Object.keys(this.app.$config.conTypes).map(key => {
				let sorts = []
				this.app.$config.conRelation[key].forEach(key2 => {
					sorts.push({
						label: this.app.$business.getType(key2),
						value: key2
					})
				})
				return {
					loading: false,
					isLastPage: false,
					isLastPages: {},
					pagings: {},
					requestTime: 0,
					currentPage: 1,
					list: [],
					sort: this.app.$config.conRelation[key].toString(),
					sorts: sorts
				}
			})
			this.data[0].loading = true
			this.$nextTick(function () {
				setTimeout(() => {
					this.onPulldown(0)
				}, 500)
			})
		},
		methods: {
			onButtonClick (e) {
				if ( e.value == 'collection' ) {
					this.app.$Router.push({
						path: '/pages/my/collection'
					})
				} else if ( e.value == 'footmark' ) {
					this.app.$Router.push({
						path: '/pages/my/footmark'
					})
				} else {
					this.app.$Router.push({
						path: '/pages/partition/index'
					})
				}
			},
			onCurrentChange (e) {
				this.current = e.current
				if ( this.data[e.current].list.length == 0 && !this.data[e.current].isLastPage ) {
					this.data[e.current].loading = true
					this.onPulldown(e.current)
				}
			},
			selectType (index) {
				this.$refs.dialog.prompt({
					title: {
						color: this.skinColor.color_1,
						text: '设置类型'
					},
					form: [{
						type: 'checkbox',
						row: true,
						wrap: true,
						value: this.data[index].sort.split(','),
						checkboxs: this.data[index].sorts.map(sort => {
							return {
								label: {
									text: sort.label,
									color: this.skinColor.color_3
								},
								value: sort.value,
								options: {
									shape: 'square',
									color: this.skinColor.color_3,
									bgColor: this.skinColor.color_bg_1,
									focus: this.skinColor.color_actived_1
								}
							}
						})
					}],
					options: {
						bgColor: this.skinColor.color_bg_2
					},
					success: res => {
						if ( res.confirm ) {
							let value = ''
							if ( res.form[0].value && res.form[0].value.length > 0 ) {
								value = res.form[0].value.toString()
							} else {
								value = this.data[index].sorts.map(sort => sort.value).toString()
							}
							this.data[index].loading = true
							this.$nextTick(function () {
								this.data[index].sort = value
								this.data[index].list = []
								this.onPulldown(index)
							})
						}
					}
				})
			},
			onPulldown (index, callback) {
				this.data[index].currentPage = 1
				this.data[index].pagings = {}
				this.data[index].isLastPages = {}
				this.data[index].requestTime = 0
				this.$refs.list && this.$refs.list[index] && this.$refs.list[index].resetLoadmore()
				this.getList(index).then(data => {
					callback && callback('success')
					this.data[index].list = data.list
					if ( data.list.length > 0 ) {
						data.isLastPage ? this.$refs.list[index].setLoadmoreEnd() : null
					} else {
						data.isLastPage ? this.$refs.list[index].setLoadmoreEnd() : this.$refs.list[index].setLoadmoreFail()
					}
					this.data[index].isLastPage = data.isLastPage
					this.data[index].pagings = data.pagings
					this.data[index].isLastPages = data.isLastPages
					this.data[index].currentPage = data.currentPage
					this.data[index].requestTime = data.requestTime
					this.data[index].loading = false
				})
			},
			onLoadmore (index, callback) {
				this.getList(index).then(data => {
					this.data[index].list = this.data[index].list.concat(data.list)
					this.data[index].isLastPage = data.isLastPage
					callback(data.isLastPage ? 'end' : data.list.length > 0 ? 'success' : 'fail')
					this.data[index].pagings = data.pagings
					this.data[index].isLastPages = data.isLastPages
					this.data[index].currentPage = data.currentPage
					this.data[index].requestTime = data.requestTime
				})
			},
			async getList (index) {
				return await this.app.$business.requestApis({
					name: 'getHotList',
					type: this.data[index].sort,
					data: {
						currentPage: this.data[index].currentPage
					},
					pagings: this.data[index].pagings,
					isLastPages: this.data[index].isLastPages,
					requestTime: this.data[index].requestTime
				}).then(data => {
					return data
				})
			}
		}
	}
</script>

<style>
	.tabs-filters {
		height: 35rpx;
		margin-right: 15rpx;
		margin-left: 15rpx;
	}
	.tabs-filters .tabs-filter {
		height: 6rpx;
		border-radius: 6rpx;
	}
	.tabs-filters .tabs-filter_1 {
		width: 20rpx;
	}
	.tabs-filters .tabs-filter_2, .tabs-filters .tabs-filter_3 {
		width: 40rpx;
	}
</style>
