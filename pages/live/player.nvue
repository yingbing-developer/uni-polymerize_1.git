<template>
	<yb-page :style="{'background-color': skinColor.color_bg_1}">
		<yb-status-bar color="rgba(0,0,0,.5)"></yb-status-bar>
		<view class="nav-bar yb-flex yb-row yb-align-center padding-gap">
			<yb-tap @click="app.$Router.back()">
				<yb-icon name="angle-arrow-left" :color="skinColor.color_1" :size="40"></yb-icon>
			</yb-tap>
			<view class="yb-flex yb-row yb-align-center yb-flex-1" style="margin-left: 20rpx;">
				<view class="yb-flex yb-row yb-align-center" @tap="characterShow = true">
					<template v-if="characters.length > 0">
						<view class="characters yb-flex yb-row yb-justify-center yb-align-center">
							<view class="character-avatar" elevation="2" v-for="(character, i) in characters.slice(0, 3)" :key="i">
								<yb-avatar :src="character.avatar || app.$business.filterSource(params.source).logo || '/static/logo.png'" size="60"></yb-avatar>
							</view>
						</view>
					</template>
					<template v-else>
						<yb-avatar :src="params.avatar || app.$business.filterSource(params.source).logo" size="60"></yb-avatar>
					</template>
				</view>
				<view class="yb-flex yb-justify-between" style="margin-left: 10rpx">
					<yb-text :lines="1" :size="24" :color="skinColor.color_1" :max="10" ellipsis :value="params.creator || app.$business.filterSource(params.source).title"></yb-text>
					<yb-text :lines="1" :size="18" :color="skinColor.color_3" :value="'当前人数：' + app.$utils.numtounit(seeing)"></yb-text>
				</view>
				<yb-tap @click="app.$business.toCollection(params)" custom-style="margin-left: 20rpx;">
					<yb-tag plain :value="app.$business.isCollection(params.id) ? '已收藏' : '收藏'" :options="{
						'bgColor': app.$business.isCollection(params.id) ? skinColor.color_3 : skinColor.color_blue,
						'radius': 30,
						'size': 20
					}"></yb-tag>
				</yb-tap>
			</view>
			<yb-tap @click="onSearchClick">
				<rd-icon name="search" :color="skinColor.color_1" :size="45"></rd-icon>
			</yb-tap>
			<yb-tap custom-style="margin-left: 15rpx" @click="app.$business.shareWeb(params)">
				<rd-icon name="share-rect" :color="skinColor.color_1" :size="40"></rd-icon>
			</yb-tap>
		</view>
		<template v-if="params.type == 'broadcast'">
			<view class="rd-live-player rd-live-broadcast yb-flex">
				<image class="rd-live-broadcast-bg" :src="params.cover" mode="aspectFit"></image>
				<view class="rd-live-broadcast-btn padding-gap yb-flex yb-flex-1 yb-row yb-align-center">
					<view class="line yb-flex-1" :style="{
						'background-color': skinColor.color_white
					}"></view>
					<yb-button @click="toggle" class="btn" :options="{
						bgColor: skinColor.color_white
					}" plain :value="ready ? paused ? '开始收听' : '正在收听' : '加载中'"></yb-button>
					<view class="line yb-flex-1" :style="{
						'background-color': skinColor.color_white
					}"></view>
				</view>
				<view class="rd-live-broadcast-title yb-flex yb-align-center">
					<yb-text :color="skinColor.color_white" :lines="1" :value="params.title" :size="33"></yb-text>
				</view>
				<view v-if="error" class="rd-live-broadcast-error yb-flex yb-row yb-justify-center yb-align-center">
					<yb-icon name="fork-circle" :color="skinColor.color_white" :size="30"></yb-icon>
					<yb-text :color="skinColor.color_white" value="加载失败" :size="30"></yb-text>
				</view>
			</view>
			<yingbing-audio ref="live" autoplay :src="params.src" @canplay="handleCanplay" @play="handlePlay" @pause="handlePause" @error="handleError"></yingbing-audio>
		</template>
		<view class="rd-live-player yb-flex" v-else>
			<video
			autoplay
			ref="live"
			enable-danmu
			is-live
			danmu-btn
			show-mute-btn
			:show-progress="false"
			:enable-progress-gesture="false"
			:show-center-play-btn="false"
			class="yb-flex-1"
			:title="title"
			:src="src">
			</video>
		</view>
		<yb-tabs custom-class="yb-flex-1" :options="options" :column="4" :data="tabs" @change="onCurrentChange">
			<template v-for="(tab, i) in tabs" :slot="i">
				<rd-cells
				v-if="tab.value == 'detail'"
				ref="recome"
				:bounce="false"
				:list="recomes"
				:loading="recomeLoading"
				pulldown
				:loadmore="recomes.length > 0 || !recomeIsLastPage"
				@pulldown="getDetail"
				@loadmore="onRecomeLoadmore">
					<yb-list-item>
						<yb-gap :size="30"></yb-gap>
					</yb-list-item>
					<yb-list-item>
						<view class="padding-gap yb-flex">
							<yb-text v-if="params.title" :lines="2" :value="params.title" :color="skinColor.color_1" :size="30"></yb-text>
							<view class="subtitle yb-flex yb-row yb-wrap yb-align-center">
								<view class="tip yb-flex yb-row yb-align-center">
									<yb-tag :options="{
										bgColor: app.$business.getColor(params.type),
										size: 20
									}" :value="app.$business.getType(params.type)"></yb-tag>
									<yb-tag custom-style="margin-left: 10rpx" :options="{
										bgColor: skinColor.color_red,
										size: 20
									}" :value="app.$business.filterSource(params.source).title"></yb-tag>
								</view>
								<view class="tip yb-flex yb-row yb-align-center">
									<rd-icon name="tv" :color="skinColor.color_3" size="30"></rd-icon>
									<text class="text" :style="{color: skinColor.color_3}">{{app.$utils.numtounit(params.readnum)}}</text>
								</view>
								<view class="tip yb-flex yb-row yb-align-center">
									<rd-icon name="record" :color="skinColor.color_3" size="22"></rd-icon>
									<text class="text" :style="{color: skinColor.color_3}">{{params.updateTime || params.createTime}}</text>
								</view>
							</view>
							<view class="subtitle yb-flex yb-row yb-wrap yb-align-center">
								<view class="tip yb-flex yb-row yb-align-center">
									<text class="text" :style="{color: skinColor.color_3}">{{params.isLiving ? '直播中' : '未开播'}}</text>
									<text class="text" v-if="params.style" :style="{color: skinColor.color_3, 'margin-left': '10rpx'}">{{params.style}}</text>
								</view>
								<view class="tip yb-flex yb-row yb-align-center">
									<rd-icon name="fire" size="25" :color="skinColor.color_3"></rd-icon>
									<text class="text" :style="{color: skinColor.color_3}">{{app.$utils.numtounit(params.popular)}}</text>
								</view>
								<view class="tip yb-flex yb-row yb-align-center">
									<rd-icon name="thumbs" size="25" :color="skinColor.color_3"></rd-icon>
									<text class="text" :style="{color: skinColor.color_3}">{{app.$utils.numtounit(params.thumbs)}}</text>
								</view>
								<view class="tip yb-flex yb-row yb-align-center">
									<rd-icon name="comment" size="25" :color="skinColor.color_3"></rd-icon>
									<text class="text" :style="{color: skinColor.color_3}">{{app.$utils.numtounit(params.comment)}}</text>
								</view>
								<view class="tip yb-flex yb-row yb-align-center">
									<rd-icon name="collection" size="25" :color="skinColor.color_3"></rd-icon>
									<text class="text" :style="{color: skinColor.color_3}">{{app.$utils.numtounit(params.collect)}}</text>
								</view>
							</view>
						</view>
					</yb-list-item>
					<yb-list-item>
						<yb-text v-if="params.desc" custom-class="padding-gap" custom-style="margin-top: 20rpx;" :lineHeight="32" :size="24" :color="skinColor.color_3" :value="params.desc"></yb-text>
					</yb-list-item>
					<yb-list-item v-if="params && params.tag.length > 0">
						<view class="padding-gap yb-flex yb-row yb-wrap yb-align-center" style="margin-top: 20rpx;">
							<yb-tap @tap="app.$business.showTaglist(tag, params.type)" v-for="(tag, i) in params.tag" :key="i">
								<yb-tag custom-style="margin-right: 20rpx;margin-bottom: 20rpx;" :options="{
									bgColor: skinColor.color_3
								}" :value="tag"></yb-tag>
							</yb-tap>
						</view>
					</yb-list-item>
					<yb-list-item>
						<yb-empty v-if="recomes.length == 0 && recomeIsLastPage" text="未找到推荐" :color="skinColor.color_gap_1" custom-style="height: 500rpx"></yb-empty>
					</yb-list-item>
				</rd-cells>
				<view class="danmu-list padding-gap yb-flex yb-flex-1" v-if="tab.value == 'interact'">
					<yb-list ref="danmu" :empty="{
						show: barrages.length == 0,
						bgColor: skinColor.color_bg_1,
						color: skinColor.color_gap_1,
						text: '暂无互动'
					}" :bounce="false" :showScrollbar="false" custom-class="yb-flex-1" type="list">
						<yb-list-item v-for="(item, index) in barrages" :key="index">
							<view class="yb-flex yb-align-start">
								<view class="danmu-list-item yb-flex yb-row yb-wrap">
									<template v-if="item.type == 'user'">
										<text class="text"
										:style="{
											color: skinColor.color_white
										}"
										v-for="(text, i) in item.name.length"
										:key="i">{{item.name.charAt(i)}}</text>
										<text class="text"
										:style="{
											color: skinColor.color_white
										}">：</text>
										<text class="text"
										:style="{
											color: item.color || skinColor.color_white
										}"
										v-for="(text, i) in item.text.length"
										:key="i">{{item.text.charAt(i)}}</text>
									</template>
									<template v-else-if="item.type == 'system'">
										<text class="text"
										:style="{
											color: skinColor.color_red
										}"
										v-for="(text, i) in item.name.length"
										:key="i">{{item.name.charAt(i)}}</text>
										<text class="text"
										:style="{
											color: skinColor.color_red
										}">：</text>
										<text class="text"
										:style="{
											color: skinColor.color_red
										}"
										v-for="(text, i) in item.text.length"
										:key="i">{{item.text.charAt(i)}}</text>
									</template>
									<template v-else>
										<text
										class="text"
										:style="{
											color: skinColor.color_orange
										}">[未知消息类型]</text>
									</template>
								</view>
							</view>
						</yb-list-item>
						<yb-list-item>
							<view style="height: 1px;" ref="bottom"></view>
						</yb-list-item>
					</yb-list>
				</view>
				<view class="yb-flex yb-flex-1" v-if="tab.value == 'sections'">
					<yb-list ref="list" :empty="{
						show: sections.length == 0,
						bgColor: skinColor.color_bg_1,
						color: skinColor.color_gap_1
					}" :bounce="false" :showScrollbar="false" custom-class="yb-flex-1" type="list">
						<yb-list-item v-for="(item, index) in sections" :key="index">
							<view class="padding-gap section-item yb-flex" :style="{
								'border-color': skinColor.color_gap_1
							}">
								<yb-text :size="24" :lines="2" :color="skinColor.color_1" :value="'节目名：' + item.title || item.desc"></yb-text>
								<yb-text custom-style="margin-top:10rpx" :size="22" :lines="1" :color="skinColor.color_3" :value="'主播：' + (item.creator || '未知')"></yb-text>
								<yb-text custom-style="margin-top:10rpx" :size="22" :lines="1" :color="skinColor.color_3" :value="'时间：' + (item.startTime || '未知') + ' 至 ' + (item.endTime || '未知')"></yb-text>
							</view>
						</yb-list-item>
					</yb-list>
				</view>
				<rd-comments
				v-if="tab.value == 'comment'"
				ref="comment"
				:list="comments"
				:loading="commentLoading"
				:empty="{
					show: comments.length == 0,
					text: '暂无留言'
				}"
				pulldown
				:loadmore="comments.length > 0"
				@pulldown="onCommentPulldown"
				@loadmore="onCommentLoadmore"></rd-comments>
			</template>
		</yb-tabs>
		<rd-characters :visible.sync="characterShow" :list="characters"></rd-characters>
		<yb-action-sheet ref="actionSheet"></yb-action-sheet>
		<yb-dialog ref="dialog"></yb-dialog>
	</yb-page>
</template>

<script>
	import appMixin from '@/common/mixin/app.js'
	import commentMixin from '@/common/mixin/comment.js'
	import recomeMixin from '@/common/mixin/recome.js'
	export default {
		mixins: [appMixin, commentMixin, recomeMixin],
		computed: {
			characters () {
				return this.params?.characters || []
			},
			options () {
				return {
					bgColor: this.skinColor.color_bg_1,
					color: this.skinColor.color_1,
					gapColor: this.skinColor.color_gap_1,
					itemColor: this.skinColor.color_gap_2,
					sliderColor: this.skinColor.color_actived_1,
					focusColor: this.skinColor.color_blue
				}
			}
		},
		data () {
			return {
				params: {},
				sections: [],
				title: '',
				src: '',
				poster: '',
				barrages: [],
				seeing: 0,
				isClose: false,
				tabs: [{
					label: '详情',
					value: 'detail'
				},{
					label: '互动',
					value: 'interact'
				},{
					label: '节目表',
					value: 'sections'
				},{
					label: '留言',
					value: 'comment'
				}],
				ready: false,
				paused: true,
				error: false
			}
		},
		onLoad() {
			this.params = JSON.parse(decodeURIComponent(getApp().globalData.$Route.query.params))
		},
		onUnload() {
			this.isClose = true
			uni.closeSocket()
		},
		beforeDestroy() {
			this.stop()
		},
		onShow () {
			this.play()
		},
		onHide() {
			if ( !this.app.isReturnBack ) this.pause()
		},
		onReady() {
			this.$nextTick(function () {
				setTimeout(() => {
					this.getDetail()
				}, 500)
			})
		},
		methods: {
			//心跳
			sendHeartbeat (heart, time = 20000) {
				if ( this.isClose ) {
					return
				}
				if ( this.heartbeatTimer ) {
					clearTimeout(this.heartbeatTimer)
					this.heartbeatTimer = null
				}
				uni.sendSocketMessage({
					data: heart,
					success: () => {
						this.heartbeatTimer = setTimeout(() => {
							this.sendHeartbeat(heart, time)
						}, time)
					},
					fail: err => {
						console.log('向服务端发送心跳失败，请检查！');
					}
				})
			},
			getDetail (callback) {
				this.app.$api.getSingleDetail(this.params).then(res => {
					if ( res.code == this.app.$config.ERR_OK ) {
						this.params = Object.assign({}, this.params, res.data.data)
						this.barrages = res.data.barrages || []
						this.sections = res.data.sections || []
					}
					if ( !this.params.shareUrl ) this.params.shareUrl = res.requestUrl
					if ( this.sections.length == 0 ) this.sections = this.params.sections
					let tag = this.app.$business.getTag(this.params)
					this.params.tag = [...new Set(this.params.tag.concat(tag))]
					this.title = this.params.title
					this.src = this.params.src
					this.poster = this.params.cover
					this.creator = this.params.creator
					if ( this.$store.getters['app/getFootmark'] ) this.$store.dispatch('record/add', this.params)
					this.onRecomePulldown(callback)
					if ( this.barrages.length == 0 ) this.barrages = this.params.barrages
					this.$nextTick(function () {
						this.scrollTimer = setTimeout(() => {
							this.$refs.danmu[0] && this.$refs.danmu[0].scrollTo(10000)
							clearTimeout(this.scrollTimer)
							this.scrollTimer = null
						}, 200)
					})
					this.app.$api.getSocketDetail(this.params).then(ret => {
						if ( ret.code == this.app.$config.ERR_OK && ret.data.data ) {
							uni.connectSocket(ret.data.data);
							uni.onSocketOpen(() => {
								console.log('WebSocket连接已打开！');
								if ( ret.data.send ) {
									uni.sendSocketMessage({
										data: ret.data.send,
										fail: err => {
											this.barrages.push({
												name: '系统消息',
												text: '向服务端发送消息失败，请检查！',
												type: 'system'
											})
											this.$nextTick(function () {
												this.scrollTimer = setTimeout(() => {
													this.$refs.danmu[0] && this.$refs.danmu[0].scrollTo(10000)
													clearTimeout(this.scrollTimer)
													this.scrollTimer = null
												}, 50)
											})
										}
									})
								}
								if ( ret.data.heartbeat ) {
									this.sendHeartbeat(ret.data.heartbeat, ret.data.heartbeattime || 20000)
								}
							})
							uni.onSocketError(() => {
								this.barrages.push({
									name: '系统消息',
									text: 'WebSocket连接打开失败，请检查！',
									type: 'system'
								})
								this.$nextTick(function () {
									this.scrollTimer = setTimeout(() => {
										this.$refs.danmu[0] && this.$refs.danmu[0].scrollTo(10000)
										clearTimeout(this.scrollTimer)
										this.scrollTimer = null
									}, 50)
								})
							})
							uni.onSocketClose(() => {
								if ( this.heartbeatTimer ) {
									clearTimeout(this.heartbeatTimer)
									this.heartbeatTimer = null
								}
								if ( !this.isClose ) {
									uni.connectSocket(ret.data.data)//尝试重连
								}
								console.log('WebSocket 已关闭！')
							})
							uni.onSocketMessage((msg) => {
								console.log('收到服务器内容：' + msg);
								this.app.$api.handleSocketMessage(msg).then(req => {
									if ( req.code == this.app.$config.ERR_OK ) {
										this.seeing = res.data.data.seeing || 0
										res.data.barrages.forEace(danmu => {
											this.$refs.live && this.$refs.live.sendDanmu(danmu)
											this.barrages.push(danmu)
										})
										if ( this.barrages.length > 100 )
										this.barrages = this.barrages.slice(this.barrages.length - 100, this.barrages.length)
										if ( this.scrollTimer ) {
											clearTimeout(this.scrollTimer)
											this.scrollTimer = null
										}
										this.$nextTick(function () {
											this.scrollTimer = setTimeout(() => {
												this.$refs.danmu[0] && this.$refs.danmu[0].scrollTo(10000)
												clearTimeout(this.scrollTimer)
												this.scrollTimer = null
											}, 100)
										})
									}
								})
							})
						} else {
							this.$nextTick(function () {
								this.scrollTimer = setTimeout(() => {
									this.$refs.danmu[0] && this.$refs.danmu[0].scrollTo(10000)
									clearTimeout(this.scrollTimer)
									this.scrollTimer = null
								}, 50)
							})
						}
					})
					if ( this.params.isPay ) {
						this.$refs.dialog.alert({
							title: {
								color: this.skinColor.color_1,
								text: '提示'
							},
							content: {
								color: this.skinColor.color_red,
								text: '付费内容！'
							},
							options: {
								bgColor: this.skinColor.color_bg_2,
								type: 2
							}
						})
						return
					}
					if ( !this.params.isLiving ) {
						this.$refs.dialog.alert({
							title: {
								color: this.skinColor.color_1,
								text: '提示'
							},
							content: {
								color: this.skinColor.color_red,
								text: '直播还未开始！'
							},
							options: {
								bgColor: this.skinColor.color_bg_2,
								type: 2
							}
						})
						return
					}
				})
			},
			onCurrentChange (e) {
				if ( this.tabs[e.current].value == 'comment' && this.comments.length == 0 ) {
					this.commentLoading = true
					this.onCommentPulldown()
				}
			},
			handleCanplay () {
				this.ready = true
			},
			handlePlay () {
				this.paused = false
			},
			handlePause () {
				this.paused = true
			},
			handleError () {
				this.error = true
			},
			play () {
				if ( this.params.src ) {
					this.$refs.live && this.$refs.live.play()
				}
			},
			toggle () {
				this.$refs.live && this.$refs.live.toggle()
			},
			pause () {
				this.$refs.live && this.$refs.live.pause()
			},
			stop () {
				this.$refs.live && this.$refs.live.stop()
			}
		}
	}
</script>

<style>
	.rd-live-player {
		position: relative;
		height: 420rpx;
	}
	.rd-live-broadcast {
		background-image: linear-gradient(to right, #e96443, #904e95);
	},
	.rd-live-broadcast-bg {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		opacity: 0.3;
	}
	.rd-live-broadcast-btn .line {
		height: 1px;
	}
	.rd-live-broadcast-btn .btn {
		width: 280rpx;
		height: 80rpx;
		margin: 0 30rpx;
	}
	.rd-live-broadcast-error {
		position: absolute;
		left: 0;
		right: 0;
		bottom: 50rpx;
	}
	.rd-live-broadcast-title {
		position: absolute;
		left: 0;
		right: 0;
		top: 50rpx;
	}
	.nav-bar {
		height: 100rpx;
	}
	.characters .character-avatar {
		border-radius: 45rpx;
	}
	.title .btn {
		width: 120rpx;
		height: 40rpx;
	}
	.title .more {
		flex-direction: row;
		align-items: center;
	}
	.title .more .text {
		font-size: 22rpx;
	}
	.subtitle {
		margin-top: 10rpx;
	}
	.tip {
		margin-right: 30rpx;
	}
	.tip .text {
		font-size: 20rpx;
		margin-left: 5rpx;
	}
	.danmu-list-vertical {
		position: absolute;
		bottom: 0;
		left: 0;
		height: 630rpx;
	}
	.danmu-list-item {
		max-width: 550rpx;
		padding: 20rpx 20rpx 10rpx 20rpx;
		border-radius: 20rpx;
		margin-bottom: 20rpx;
		background-color: rgba(0,0,0,.15);
	}
	.danmu-list .text {
		font-size: 24rpx;
		margin-bottom: 10rpx;
	}
	.section-item {
		padding-top: 20rpx;
		padding-bottom: 20rpx;
		border-bottom-width: 1px;
		border-bottom-style: solid;
	}
</style>