<template>
	<yb-page :style="{'background-color': skinColor.color_bg_1}">
		<view class="nav-bar yb-flex">
			<yb-nav-bar
			shape="circle"
			title="详情"
			:scrollTop="scrollTop"
			transparent
			transparentTitle
			:options="{
				bgColor: skinColor.color_theme_1,
				color: skinColor.color_4,
				style: skinMode == 'night' ? 'white' : 'black'
			}">
			</yb-nav-bar>
		</view>
		<rd-cells
		enableScrolling
		@scroll="onScroll"
		ref="list"
		:list="recomes"
		:loading="recomeLoading"
		pulldown
		:loadmore="!recomeIsLastPage || recomes.length > 0"
		@pulldown="onPulldown"
		@loadmore="onRecomeLoadmore">
			<yb-list-item>
				<view class="top yb-flex">
					<view class="mask"></view>
					<rd-image class="bg" :src="app.$business.getCover(params)" mode="aspectFill"></rd-image>
					<yb-gap :size="top + 'px'"></yb-gap>
					<view class="info yb-flex yb-row yb-align-start">
						<rd-image :enableMask="false" class="cover" :src="app.$business.getCover(params)" mode="aspectFill"></rd-image>
						<view class="info-right">
							<yb-text weight="bold" :lineHeight="35" :size="30" :lines="2" :color="skinColor.color_white" :value="params.title" />
							<yb-text class="label" :size="22" :lines="1" :color="skinColor.color_white" :value="'作者：' + params.creator + ' / 著'" />
							<yb-text class="label" :size="22" :lines="1" :color="skinColor.color_white" :value="'类型：' + (params.style || '未知类型')" />
							<yb-text class="label" :size="22" :lines="1" :color="skinColor.color_white" v-if="params.size" :value="'大小：' + params.size" />
							<yb-text class="label" :size="22" :lines="1" :color="skinColor.color_white" :value="'状态：' + (params.isEnd ? '已完结' : '连载中')" />
							<yb-text class="label" :size="22" :lines="1" :color="skinColor.color_white" :value="'来源：' + app.$business.filterSource(params.source).title" />
						</view>
					</view>
					<view class="line yb-flex yb-row yb-align-center yb-wrap">
						<yb-tap custom-style="margin-right: 10rpx;margin-bottom: 10rpx;" @click="app.$business.showTaglist(tag, params.type)" v-for="(tag, i) in params.tag" :key="i">
							<yb-tag
							:options="{
								size: 20
							}"
							:value="tag"></yb-tag>
						</yb-tap>
					</view>
				</view>
			</yb-list-item>
			<yb-list-item v-if="params && params.characters.length > 0">
				<view class="box">
					<view class="column" :style="{'border-color': skinColor.color_cyan}">
						<text class="text" :style="{color: skinColor.color_1}">作者</text>
					</view>
					<yb-gap size="30rpx"></yb-gap>
					<rd-character :list="params.characters"></rd-character>
				</view>
				<yb-gap size="10rpx" :color="skinColor.color_gap_1"></yb-gap>
			</yb-list-item>
			<yb-list-item>
				<view class="box">
					<view class="column" :style="{'border-color': skinColor.color_cyan}">
						<text class="text" :style="{color: skinColor.color_1}">简介</text>
					</view>
					<text class="desc" :style="{color: skinColor.color_2}">{{params.desc || '暂无简介'}}</text>
				</view>
				<yb-gap size="10rpx" :color="skinColor.color_gap_1"></yb-gap>
			</yb-list-item>
			<yb-list-item v-if="record">
				<view class="box">
					<view class="column" :style="{'border-color': skinColor.color_cyan}">
						<text class="text" :style="{color: skinColor.color_1}">正在阅读</text>
						<yb-tap @click="deleteRecord">
							<rd-icon name="dustbin" size="30" :color="skinColor.color_2"></rd-icon>
						</yb-tap>
					</view>
					<view class="chapter yb-flex yb-row yb-align-center" @tap="createReader(item)">
						<yb-text class="label" :lines="1" :size="26" :color="skinColor.color_1" :value="record.title" />
						<rd-icon name="arrow-right" size="30" :color="skinColor.color_2"></rd-icon>
					</view>
				</view>
				<yb-gap size="10rpx" :color="skinColor.color_gap_1"></yb-gap>
			</yb-list-item>
			<yb-list-item>
				<view class="box">
					<view class="column" :style="{'border-color': skinColor.color_cyan}">
						<text class="text" :style="{color: skinColor.color_1}">最新章节</text>
						<view class="more opac-actived" v-if="params.updateTime">
							<text class="text" :style="{color: skinColor.color_2}">（更新时间：{{params.updateTime}}）</text>
						</view>
					</view>
					<yb-tap class="chapter yb-flex yb-row yb-align-center" @click="createReader(item)" v-for="(item, index) in chapters.filter(cha => cha.id).slice(-5).reverse()" :key="index">
						<yb-text class="label" :lines="1" :size="26" :color="skinColor.color_1" :value="item.title" />
						<yb-text :color="skinColor.color_red" size="26" value="付费" v-if="item.isPay"></yb-text>
						<rd-icon name="arrow-right" size="30" :color="skinColor.color_2" v-if="item.chapter && !item.isPay"></rd-icon>
					</yb-tap>
				</view>
				<yb-gap size="10rpx" :color="skinColor.color_gap_1"></yb-gap>
			</yb-list-item>
			<yb-list-item>
				<view class="box">
					<view class="column" :style="{'border-color': skinColor.color_cyan}">
						<text class="text" :style="{color: skinColor.color_1}">目录</text>
						<yb-tap custom-class="more" @click="showCatalog">
							<text class="text" :style="{color: skinColor.color_2}">共{{chapters.filter(ca => ca.chapter).length}}章</text>
							<rd-icon name="arrow-right" size="30" :color="skinColor.color_2"></rd-icon>
						</yb-tap>
					</view>
					<yb-tap class="chapter yb-flex yb-row yb-align-center" @click="createReader(item)" v-for="(item, index) in chapters.slice(0, 10)" :key="index">
						<yb-text class="label" :lines="1" :size="26" :color="skinColor.color_1" :value="item.title" />
						<yb-text :color="skinColor.color_red" size="26" value="付费" v-if="item.isPay"></yb-text>
						<rd-icon name="arrow-right" size="30" :color="skinColor.color_2" v-if="item.chapter && !item.isPay"></rd-icon>
					</yb-tap>
				</view>
				<yb-gap size="10rpx" :color="skinColor.color_gap_1"></yb-gap>
			</yb-list-item>
			<yb-list-item>
				<view class="box">
					<view class="column" :style="{'border-color': skinColor.color_cyan}">
						<text class="text" :style="{color: skinColor.color_1}">为你推荐</text>
					</view>
					<yb-empty v-if="recomeIsLastPage && recomes.length == 0" custom-style="height: 400rpx" :color="skinColor.color_gap_1" text="未找到推荐"></yb-empty>
				</view>
			</yb-list-item>
		</rd-cells>
		<view class="bottom padding-gap yb-flex yb-row yb-align-center yb-justify-end" :style="{'background-color': skinColor.color_bg_2, 'border-top': '1px solid ' + skinColor.color_gap_1}">
			<view class="yb-flex yb-flex-1 yb-row yb-align-center yb-justify-center">
				<view class="yb-flex yb-align-center yb-flex-1" @tap="app.$business.showComment(params)">
					<rd-icon name="comment" size="40" :color="skinColor.color_1"></rd-icon>
					<text class="label" :style="{color: skinColor.color_1}">评论</text>
				</view>
				<view class="yb-flex yb-align-center yb-flex-1" @tap="onSearchClick">
					<rd-icon name="search" size="40" :color="skinColor.color_1"></rd-icon>
					<text class="label" :style="{color: skinColor.color_1}">搜索</text>
				</view>
				<view class="yb-flex yb-align-center yb-flex-1" @tap="app.$business.toCollection(params)">
					<rd-icon
					:name="app.$business.isCollection(params.id) ? 'love-fill' : 'love'"
					size="40"
					:color="app.$business.isCollection(params.id) ? skinColor.color_red : skinColor.color_1"></rd-icon>
					<text class="label" :style="{color: skinColor.color_1}">收藏</text>
				</view>
				<view class="yb-flex yb-align-center yb-flex-1" @tap="app.$business.shareWeb(params)">
					<rd-icon name="share-rect" size="40" :color="skinColor.color_1"></rd-icon>
					<text class="label" :style="{color: skinColor.color_1}">分享</text>
				</view>
			</view>
			<view style="width: 40rpx;"></view>
			<yb-button @click="createReader(null)" custom-class="btn" :value="record  ?  '继续阅读' : '立即阅读'"></yb-button>
		</view>
		<rd-catalog ref="catalog" type="chapter" :title="params.title" @itemClick="onChapterClick"></rd-catalog>
		<yb-dialog ref="dialog"></yb-dialog>
		<yb-action-sheet ref="actionSheet"></yb-action-sheet>
	</yb-page>
</template>

<script>
	import appMixin from '@/common/mixin/app.js'
	import recomeMixin from '@/common/mixin/recome.js'
	export default {
		mixins: [appMixin, recomeMixin],
		data () {
			return {
				params: '',
				scrollTop: 0,
				top: 0,
				fontFace: {},
				chapters: []
			}
		},
		computed: {
			title () {
				return this.params?.title || '书籍详情'
			},
			record () {
				let index = this.$store.getters['record/get'].findIndex(record => record.id == this.params.id)
				return index > -1 ? this.$store.getters['record/get'][index].record : null
			}
		},
		onShow() {
			plus.screen.lockOrientation('portrait-primary');
		},
		onReady() {
			this.params = JSON.parse(decodeURIComponent(this.app.$Route.query.params || ''))
			this.top = (uni.getSystemInfoSync().safeArea.top + uni.upx2px(100))
			this.$nextTick(function () {
				setTimeout(() => {
					this.onPulldown()
				}, 500)
			})
		},
		methods: {
			onScroll (e) {
				this.scrollTop = e.scrollTop
			},
			createReader (item) {
				if ( this.chapters.length == 0 ) {
					return
				}
				if ( !item ) {
					let chapters = this.chapters.filter(ca => ca.chapter)
					let index = chapters.findIndex(ca => ca.chapter == this.record?.chapter)
					item = chapters[index > -1 ? index : 0]
				}
				if ( !item?.chapter ) {
					return
				}
				if ( item.isPay ) {
					this.$refs.dialog.alert({
						title: {
							color: this.skinColor.color_1,
							text: '提示'
						},
						content: {
							color: this.skinColor.color_2,
							text: '该章节需要付费或VIP才能阅读！\n\n请支持正版'
						},
						options: {
							bgColor: this.skinColor.color_bg_2
						}
					})
					return
				}
				this.$store.dispatch('reader/init', {
					book: this.params,
					chapters: this.chapters,
					record: {
						title: item.title,
						chapter: item.chapter,
						start: this.record && this.record.chapter == item.chapter ? this.record.start : 0
					}
				})
				this.app.$Router.push({
					path: this.params.type == 'novel' ? '/pages/book/reader' : '/pages/comic/reader'
				})
			},
			onPulldown (callback) {
				this.app.$api.getSingleDetail(this.params).then((res) => {
					if ( res.code == this.app.$config.ERR_OK ) {
						callback && callback('success')
						this.params = Object.assign({} , this.params, res.data.data || {})
						let tag = this.app.$business.getTag(this.params)
						this.params.tag = [...new Set(this.params.tag.concat(tag))]
						this.chapters = res.data.sections || []
					} else {
						callback && callback('fail')
					}
					if ( !this.params.shareUrl ) this.params.shareUrl = res.requestUrl
					if ( this.chapters.length == 0 ) this.chapters = this.params.sections
					if ( this.params.isPay ) {
						this.$refs.dialog.alert({
							title: {
								color: this.skinColor.color_1,
								text: '提示'
							},
							content: {
								color: this.skinColor.color_red,
								text: '付费资源！请支持正版'
							},
							options: {
								bgColor: this.skinColor.color_bg_2,
								type: 2
							}
						})
					}
					this.onRecomePulldown()
				})
			},
			showCatalog () {
				this.$store.dispatch('reader/init', {
					book: this.params,
					chapters: this.chapters
				})
				this.$refs.catalog.show()
			},
			onChapterClick (chapter) {
				this.$refs.catalog.hide()
				this.createReader(chapter)
			},
			deleteRecord () {
				this.$refs.dialog.confirm({
					title: {
						color: this.skinColor.color_1,
						text: '提示'
					},
					content: {
						color: this.skinColor.color_2,
						text: '删除阅读记录？\n\n点击确认继续'
					},
					options: {
						bgColor: this.skinColor.color_bg_2
					},
					success: res => {
						if ( res.confirm ) {
							this.$store.dispatch('record/remove', this.params.id)
						}
					}
				})
			}
		}
	}
</script>

<style>
	.nav-bar {
		position: fixed;
		top: 0;
		left: 0;
		right: 0;
	}
	.top {
		position: relative;
	}
	.top .mask {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: rgba(0,0,0,.5);
	}
	.top .bg {
		position: absolute;
		left: 0;
		top: 0;
		bottom: 0;
		right: 0;
		opacity: .05;
	}
	.top .info {
		padding: 0 30rpx 10rpx 30rpx;
		position: relative;
		z-index: 1;
	}
	.top .info .cover {
		width: 180rpx;
		height: 240rpx;
	}
	.top .info .info-right {
		display: flex;
		flex-direction: column;
		margin-left: 20rpx;
		flex: 1;
	}
	.top .line {
		position: relative;
		z-index: 1;
		padding: 0 30rpx 10rpx 30rpx;
	}
	.top .info .label {
		margin-top: 15rpx;
	}
	.nav-bar {
		position: fixed;
		left: 0;
		top: 0;
		right: 0;
		z-index: 2;
	}
	.box {
		padding: 30rpx;
		display: flex;
		flex-direction: column;
	}
	.box .column {
		display: flex;
		flex-direction: row;
		align-items: center;
		height: 40rpx;
		justify-content: space-between;
		border-left-width: 10rpx;
		border-left-style: solid;
	}
	.box .column .more {
		flex-direction: row;
		align-items: center;
	}
	.box .column .more .text {
		font-size: 24rpx;
		margin-right: 5rpx;
	}
	.box .column .text {
		font-size: 26rpx;
		margin-left: 20rpx;
	}
	.box .desc {
		font-size: 26rpx;
		margin-top: 30rpx;
	}
	.box .chapter {
		margin-top: 30rpx;
		padding: 20rpx 0;
	}
	.box .chapter .icon {
		margin-right: 10rpx;
	}
	.box .chapter .label {
		flex: 1;
		lines: 1;
		text-overflow: ellipsis;
		font-size: 28rpx;
	}
	.bottom {
		height: 120rpx;
	}
	.bottom .label {
		font-size: 24rpx;
		margin-top: 10rpx;
	}
	.bottom .btn {
		width: 200rpx;
		height: 60rpx;
	}
</style>
