import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app.js'
import user from './modules/user.js'
import search from './modules/search.js'
import cache from './modules/cache.js'
import record from './modules/record.js'
import source from './modules/source.js'
import referer from './modules/referer.js'
import collection from './modules/collection.js'
import path from './modules/path.js'
import audio from './modules/audio.js'
import reader from './modules/reader.js'
import debug from './modules/debug.js'
import browser from './modules/browser.js'
Vue.use(Vuex)
const store = new Vuex.Store({
	modules: {
		app,
		user,
		search,
		cache,
		record,
		source,
		referer,
		collection,
		path,
		audio,
		reader,
		debug,
		browser
	}
})
export default store