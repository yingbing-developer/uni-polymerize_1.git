import { DEBUG } from '../config.js'

const state = {
	debug: uni.getStorageSync(DEBUG) || false, //调试模式
	networks: []//网络请求
}

const getters = {
	get (state) {
		return state.debug
	},
	getNetwork (state) {
		return state.networks
	}
}

const mutations = {
	set (state, bol) {
		state.debug = bol;
		uni.setStorageSync(DEBUG, bol)
	},
	setNetwork (state, list) {
		state.networks = list;
	}
}

const actions = {
	//切换调试模式
	switch ({state, commit}) {
		commit('set', !state.debug)
	},
	incNetwork ({state, commit}, network) {
		let list = [...state.networks]
		list.unshift(network)
		commit('setNetwork', list)
	},
	removeNetwork ({state, commit}, index) {
		let list = [...state.networks]
		list.splice(index, 1)
		commit('setNetwork', list)
	},
	clearNetwork ({commit}) {
		commit('setNetwork', [])
	}
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
	actions
}