import { RECORD } from '../config.js'

const state = {
	record: uni.getStorageSync(RECORD) || [] //记录列表
}

const getters = {
	get (state) {
		return state.record
	}
}

const mutations = {
	set (state, record) {
		state.record = record
		uni.setStorageSync(RECORD, state.record)
	}
}

const actions = {
	add ({state, commit}, record) {
		let list = [...state.record]
		let index = list.findIndex(item => item.id == record.id)
		if ( index > -1 ) {
			list.splice(index, 1)
			list.unshift(record)
		} else {
			list.unshift(record)
		}
		commit('set', list)
	},
	remove ({state, commit}, id) {
		let list = [...state.record]
		let index = list.findIndex(item => item.id == id)
		if ( index > -1 ) list.splice(index, 1);
		commit('set', list)
	},
	clear ({state, commit}, type) {
		let list = state.record.filter(item => item.type != type)
		commit('set', list)
	}
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
	actions
}