import { PLAYMODE, LYRICSHOW } from '../config.js'
//音乐播放
const state = {
	currentAlbum: null,//当前专辑
	currentPage: 1,//当前专辑请求页数
	currentPaging: {},//当前专辑请求页信息
	currentIsLastPage: false,//当前专辑请求是否最后一页
	playList: [],//播放列表
	randomPlayList: [],//随机播放列表
	currentSong: null,//当前播放歌曲
	currentLyricTitle: '',//当前歌词
	currentLyricIndex: -1,//当前歌词索引
	paused: true,//是否暂停
	currentTime: 0,//播放时间
	duration: 1,//歌曲长度
	playMode: uni.getStorageSync(PLAYMODE) || 'round', //播放模式
	lyricShow: uni.getStorageSync(LYRICSHOW) || false, //是否显示全局歌词
}

const getters = {
	getCurrentAlbum (state) {
		return state.currentAlbum
	},
	getCurrentPage (state) {
		return state.currentPage
	},
	getCurrentPaging (state) {
		return state.currentPaging
	},
	getCurrentIsLastPage (state) {
		return state.currentIsLastPage
	},
	getPlayList (state) {
		return state.playList
	},
	getRandomPlayList (state) {
		return state.randomPlayList
	},
	getCurrentSong (state) {
		return state.currentSong
	},
	getCurrentLyricTitle (state) {
		return state.currentLyricTitle
	},
	getCurrentLyricIndex (state) {
		return state.currentLyricIndex
	},
	getPaused (state) {
		return state.paused
	},
	getPlayMode (state) {
		return state.playMode;
	},
	getLyricShow (state) {
		return state.lyricShow;
	},
	getCurrentTime (state) {
		return state.currentTime
	},
	getDuration (state) {
		return state.duration
	}
}

const mutations = {
	//设置当前专辑
	setCurrentAlbum (state, album) {
		state.currentAlbum = album || null
	},
	//设置请求页数
	setCurrentPage (state, page) {
		state.currentPage = page
	},
	//设置请求页信息
	setCurrentPaging (state, paging) {
		state.currentPaging = paging
	},
	//设置请求是否最后一页
	setCurrentIsLastPage (state, isLastPage) {
		state.currentIsLastPage = isLastPage
	},
	//设置播放列表
	setPlayList (state, playList) {
		state.playList = playList || []
	},
	//设置随机播放列表
	setRandomPlayList (state, playList) {
		state.randomPlayList = playList || []
	},
	//设置当前播放歌曲
	setCurrentSong (state, song) {
		state.currentSong = song || null
	},
	//设置当前歌词
	setCurrentLyricTitle (state, title) {
		state.currentLyricTitle = title || ''
	},
	//设置当前歌词索引
	setCurrentLyricIndex (state, index) {
		state.currentLyricIndex = index
	},
	//设置当前播放状态
	setPaused (state, bol) {
		state.paused = bol
	},
	//设置当前播放时间
	setCurrentTime (state, time) {
		state.currentTime = time || 0
	},
	//设置当前歌曲长度
	setDuration (state, time) {
		state.duration = time
	},
	//设置播放模式
	setPlayMode (state, playMode) {
		state.playMode = playMode || 'round'
		uni.setStorageSync(PLAYMODE, state.playMode)
	},
	//设置是否显示全局歌词
	setLyricShow (state, bol) {
		state.lyricShow = bol
		uni.setStorageSync(LYRICSHOW, state.lyricShow)
	},
}

const actions = {
	//增加歌曲
	addSong ({state, commit}, songs) {
		let list = [...state.playList]
		const newSongs = songs.filter(song => {
			return !song.payplay && list.findIndex(item => item.id == song.id) == -1
		})
		list = list.concat(newSongs)
		commit('setPlayList', list)
	},
	//移除指定歌曲
	removeSong ({state, commit}, id) {
		const list = [...state.playList]
		const songIndex = list.findIndex(item => item.id == id)
		list.splice(songIndex, 1)
		commit('setPlayList', list)
	},
	//切换播放模式
	switchPlayMode ({state, commit, dispatch}) {
		switch ( state.playMode ) {
			case 'round':
				commit('setPlayMode', 'loop')
				break
			case 'loop':
				commit('setPlayMode', 'random')
				break
			default:
				commit('setPlayMode', 'round')
				break
		}
	},
	//切换歌词显示
	switchLyricShow ({state, commit}) {
		commit('setLyricShow', !state.lyricShow)
	},
	prev ({state, commit}) {
		if ( state.playMode != 'loop' ) {
		let list = state.playMode == 'random' ? state.randomPlayList : state.playList
		let index = list.findIndex(item => item.id == state.currentSong?.id)
		if ( index > -1 ) { commit('setCurrentSong', list[index - 1 < 0 ? list.length - 1 : index - 1]) }
		else { list.length > 0 ? commit('setCurrentSong', list[0]) : commit('setCurrentSong', null) }
		} else { uni.$emit('seekSong', 0) }
	},
	next ({state, commit}) {
		if ( state.playMode != 'loop' ) {
			let list = state.playMode == 'random' ? state.randomPlayList : state.playList
			let index = list.findIndex(item => item.id == state.currentSong?.id)
			if ( index > -1 ) { commit('setCurrentSong', list[index + 1 > list.length - 1 ? 0 : index + 1]) }
			else { list.length > 0 ? commit('setCurrentSong', list[0]) : commit('setCurrentSong', null) }
		} else { uni.$emit('seekSong', 0) }
	},
	//停止音频播放
	stop ({commit}) {
		commit('setCurrentSong', null)
		commit('setCurrentLyricTitle', '')
		commit('setCurrentLyricIndex', -1)
		commit('setPaused', true)
		commit('setCurrentTime', 0)
		commit('setDuration', 1)
	},
	//销毁音频
	destroy ({state, commit}) {
		commit('setCurrentAlbum', null)
		commit('setPlayList', [])
		commit('setRandomPlayList', [])
		commit('setCurrentSong', null)
		commit('setCurrentLyricTitle', '')
		commit('setCurrentLyricIndex', -1)
		commit('setPaused', true)
		commit('setCurrentTime', 0)
		commit('setDuration', 1)
	}
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
	actions
}
