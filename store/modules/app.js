import { ADULT, ADULTPWD, SKIN, FOOTMARK, VIDEOPLAYER } from '../config.js'
import SkinColor from '@/assets/skin/index.js'

const state = {
	adult: uni.getStorageSync(ADULT) || false, //青壮年模式
	adultPwd: uni.getStorageSync(ADULTPWD) || '', //青壮年模式密码
	skin: uni.getStorageSync(SKIN) || 'default', //皮肤
	footmark: uni.getStorageSync(FOOTMARK).toString() == 'false' ? false : true, //浏览历史开关 默认开启
	videoplayer: uni.getStorageSync(VIDEOPLAYER) || '' //视频播放器
}

const getters = {
	getAdult (state) {
		return state.adult
	},
	getAdultPwd (state) {
		return state.adultPwd
	},
	getFootmark (state) {
		return state.footmark
	},
	getVideoplayer () {
		return state.videoplayer
	},
	skinMode (state) {
		return state.skin
	},
	skinColor (state) {
		return SkinColor[state.skin]
	}
}

const mutations = {
	setAdult (state, bol) {
		state.adult = bol;
		uni.setStorageSync(ADULT, bol)
	},
	setAdultPwd (state, pwd) {
		state.adultPwd = pwd;
		uni.setStorageSync(ADULTPWD, pwd)
	},
	setFootmark (state, bol) {
		state.footmark = bol;
		uni.setStorageSync(FOOTMARK, bol)
	},
	setVideoplayer (state, player) {
		state.videoplayer = player;
		uni.setStorageSync(VIDEOPLAYER, player)
	},
	setSkin (state, skin) {
		state.skin = skin;
		uni.setStorageSync(SKIN, skin)
	}
}

const actions = {
	//改变皮肤模式
	switchSkin ({state, commit}) {
		commit('setSkin', state.skin == 'default' ? 'night' : 'default')
		const Business = require('@/assets/js/business.js').default
		Business.setSkinColor()
	},
	changeAdultPwd ({commit}, pwd) {
		commit('setAdultPwd', pwd)
	},
	switchAdult ({state, commit}) {
		commit('setAdult', !state.adult)
	},
	//切换浏览历史开关状态
	switchFootmark ({state, commit}) {
		commit('setFootmark', !state.footmark)
	}
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
	actions
}