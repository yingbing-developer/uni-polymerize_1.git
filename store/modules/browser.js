import { STARTPAGE, BROWSERSHIELD, BROWSER, BLOCKNETWORKIMAGE } from '../config.js'

const state = {
	startpage: uni.getStorageSync(STARTPAGE) || 'https://www.baidu.com', //启动页
	shield: uni.getStorageSync(BROWSERSHIELD) || '', //屏蔽规则
	blockNetworkImage: uni.getStorageSync(BLOCKNETWORKIMAGE) || false,//无图模式
	browser: uni.getStorageSync(BROWSER) || [] //书签/历史/脚本列表
}

const getters = {
	getStartpage (state) {
		return state.startpage
	},
	getShield (state) {
		return state.shield
	},
	getBrowser (state) {
		return state.browser
	},
	getBlockNetworkImage (state) {
		return state.blockNetworkImage
	}
}

const mutations = {
	setStartpage (state, page) {
		state.startpage = page
		uni.setStorageSync(STARTPAGE, state.startpage)
	},
	setShield (state, shield) {
		state.shield = shield
		uni.setStorageSync(BROWSERSHIELD, state.shield)
	},
	setBrowser (state, browser) {
		state.browser = browser
		uni.setStorageSync(BROWSER, state.browser)
	},
	setBlockNetworkImage(state, bol) {
		state.blockNetworkImage = bol
		uni.setStorageSync(BLOCKNETWORKIMAGE, state.blockNetworkImage)
	}
}

const actions = {
	addBrowser ({state, commit}, params) {
		const list = [...state.browser]
		const index = list.findIndex(item => item.id == params.id)
		index > -1 ? list[index] = params : list.push(params)
		commit('setBrowser', list)
	},
	removeBrowser ({state, commit}, id) {
		const list = [...state.browser]
		const index = list.findIndex(item => item.id == id)
		if ( index > -1 ) list.splice(index, 1);
		commit('setBrowser', list)
	},
	clearBrowser ({state, commit}, type) {
		commit('setBrowser', state.browser.filter(browser => browser.type != type))
	},
	switchBlockNetworkImage ({ state, commit }) {
		commit('setBlockNetworkImage', !state.blockNetworkImage)
	}
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
	actions
}