import { mapGetters, mapMutations, mapActions } from 'vuex'
const appMixin = {
	computed: {
		...mapGetters({
			getAdult: 'app/getAdult',
			getAdultPwd: 'app/getAdultPwd',
			skinMode: 'app/skinMode',
			skinColor: 'app/skinColor'
		}),
		app () {
			return getApp().globalData
		}
	},
	methods: {
		...mapMutations({
			setAdult: 'app/setAdult',
			setAdultPwd: 'app/setAdultPwd'
		}),
		...mapActions({
			changeSkin: 'app/switchSkin'
		}),
		isBack () {
			this.$refs.dialog.confirm({
				title: {
					color: this.skinColor.color_1,
					text: '退出提示'
				},
				content: {
					text: '要退出应用吗？再次点击返回键取消',
					color: this.skinColor.color_2
				},
				confirm: "后台运行",
				cancel: "直接退出",
				options: {
					bgColor: this.skinColor.color_bg_2,
					type: 3
				},
				success: (res) => {
					getApp().globalData.backTime = 0
					if ( res.confirm ) {
						plus.android.runtimeMainActivity().moveTaskToBack(false);
					} else {
						getApp().globalData.backTime = 2
						plus.runtime.quit()
					}
				},
				fail: () => {
					getApp().globalData.backTime = 0
				}
			})
		},
		onSearchClick (args) {
			let params = this.params || args
			if ( !params ) return
			this.$refs.actionSheet.show({
				title: {
					text: '更多操作',
					color: this.skinColor.color_2
				},
				itemList: [{
					label: '搜索标题',
					value: 'searchtitle'
				},{
					label: '搜索创建者',
					value: 'searchcreator'
				},{
					label: '复制标题',
					value: 'copytitle'
				},{
					label: '复制创建者',
					value: 'copycreator'
				}],
				options: {
					cancel: {
						text: '关闭',
						color: this.skinColor.color_1
					},
					bgColor: this.skinColor.color_bg_2,
					color: this.skinColor.color_1,
					border: {
						color: this.skinColor.color_gap_1,
					}
				},
				success: (res) => {
					if ( res.confirm ) {
						if ( res.data.detail.value == 'searchtitle' ) {
							this.app.$business.toSearch(params.title, params.type)
						}
						if ( res.data.detail.value == 'searchcreator' ) {
							if ( params.creator ) {
								this.app.$business.toSearch(params.creator, params.type)
							} else {
								uni.showToast({
									icon: 'none',
									title: '未找到创建者'
								})
							}
						}
						if ( res.data.detail.value == 'copytitle' ) {
							uni.setClipboardData({
								data: params.title,
								success: function () {
									uni.showToast({
										icon: 'none',
										title: '标题已复制'
									})
								}
							})
						}
						if ( res.data.detail.value == 'copycreator' ) {
							if ( params.creator ) {
								uni.setClipboardData({
									data: params.creator,
									success: function () {
										uni.showToast({
											icon: 'none',
											title: '创建者名称已复制'
										})
									}
								})
							} else {
								uni.showToast({
									icon: 'none',
									title: '未找到创建者'
								})
							}
						}
					}
				}
			})
		}
	}
}

export default appMixin;